package com.core.sevice;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ObjectUtils;

import com.hibernate.config.OSet;
import com.jbdc.config.ConfigManager;
import com.jbdc.config.DataSet;

public abstract class DB_Module {
	public static String dataname_db = ConfigManager.getProperty("com.dano.j4F.jdbc.DataSet.dataname_db");
	public static String username_db = ConfigManager.getProperty("com.dano.j4F.jdbc.DataSet.username_db");
	public static String password_db = ConfigManager.getProperty("com.dano.j4F.jdbc.DataSet.password_db");
	public static String hostname_db = ConfigManager.getProperty("com.dano.j4F.jdbc.DataSet.hostname_db");
	private static List<Map<String, Object>> listTableCache;

	public static DataSet getDataSet() {
		DataSet DataSet = new DataSet();
		DataSet.setDB_DATABASE_NAME(dataname_db);
		DataSet.setDB_USERNAME(username_db);
		DataSet.setDB_PASSWORD(password_db);
		DataSet.setHOSTNAME(hostname_db);
		return DataSet;
	}

	private static List<Map<String, Object>> getListOsetSetting() {
		if (listTableCache == null) {
			DataSet ds = getDataSet();
			listTableCache = ds.searchAndRetrieve("SELECT * FROM sys_class_hibernate");
		}
		return listTableCache;
	}

	public static OSet getOsetC() {
		OSet oSet = new OSet();
		oSet = OSet.initC(username_db, password_db, hostname_db, "3306", dataname_db);
		return oSet;
	}

	public static OSet getOsetU() {
		OSet oSet = new OSet();
		oSet = OSet.initU(username_db, password_db, hostname_db, "3306", dataname_db);
		return oSet;
	}

	public static OSet getOset() {
		OSet oSet = new OSet();
		oSet = OSet.initU(username_db, password_db, hostname_db, "3306", dataname_db);
		List<Map<String, Object>> li = getListOsetSetting();
		for (Map<String, Object> map : li) {
			oSet.addClass(ObjectUtils.toString(map.get("class_name")));
		}
		oSet.resignAll();
		return oSet;
	}

	@SuppressWarnings("deprecation")
	public synchronized static String get999config(String key) {
		DataSet ds = getDataSet();
		ds.setField("PARAM_NAME", key);
		List<Map<String, Object>> li = ds
				.searchAndRetrieve("Select PARAM_VALUE FROM A999_CONFIG WHERE PARAM_NAME=:PARAM_NAME");
		if (li != null) {
			if (li.size() > 0) {
				for (Map<String, Object> map : li) {
					return ObjectUtils.toString(map.get("param_value"), "");
				}
			}
		}

		return null;
	}

	public static int set999config(String key, String value) {
		DataSet ds = getDataSet();
		ds.setField("PARAM_NAME", key);
		ds.setField("PARAM_VALUE", value);
		return ds.update(
				"INSERT INTO A999_CONFIG (PARAM_NAME, PARAM_VALUE, DESCRIPTION) VALUES(:PARAM_NAME, :PARAM_VALUE, NULL);");
	}

	public static boolean isExitsTable(String Table) {
		boolean check = true;
		DataSet ds = getDataSet();
		ds.setField("TABLE", Table);
		if (ds.searchAndRetrieve("SHOW TABLES LIKE :TABLE").size() > 0) {
			check = false;
		}
		ds.setField("TABLE", Table.toLowerCase());
		if (ds.searchAndRetrieve("SHOW TABLES LIKE :TABLE").size() == 0) {
			check = true;
		}
		return check;
	}

	public static void main(String[] args) {
		set999config("test", "1234");
		// System.out.println(getListOsetSetting());
	}

}
