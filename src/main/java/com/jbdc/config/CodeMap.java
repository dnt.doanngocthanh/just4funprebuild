package com.jbdc.config;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.core.sevice.DB_Module;

public class CodeMap {
	private static List<Map<String, Object>> codeMaps;
	private static String searchCodeMap = ConfigManager.getProperty("com.dano.j4F.jdbc.DataSet.sql");
	private static boolean cache = false;
	private static DataSet ds;
	static {
		codeMaps = new ArrayList<Map<String, Object>>();
		ds = getDataSet();
		if(!searchCodeMap.isEmpty()) {
			codeMaps = ds.searchAndRetrieve(searchCodeMap);
		}
		
	}

	public void reNewData() {
		if (codeMaps == null) {
			codeMaps = ds.searchAndRetrieve(searchCodeMap);
		}
	}

	public void clearCacheCodeMap() {
		codeMaps.clear();
		if (cache) {
			reNewData();
			cache = false;
		} else {
			cache = true;
			clearCacheCodeMap();
		}
	}

	public static DataSet getDataSet() {
		DataSet DataSet = new DataSet();
		DataSet.setDB_DATABASE_NAME(DB_Module.dataname_db);
		DataSet.setDB_USERNAME(DB_Module.username_db);
		DataSet.setDB_PASSWORD(DB_Module.password_db);
		DataSet.setHOSTNAME(DB_Module.hostname_db);
		return DataSet;
	}

	public static String getSQLString(String keySql) {
		for (Map<String, Object> map : codeMaps) {
			if (keySql.equals(map.get("code_name").toString())) {
				return map.get("sqlstring").toString();
			}
		}
		if (!keySql.equals(searchCodeMap)) {
			System.err.println("[WARNING] sql key not found in database: " + keySql);
		}

		return "";
	}

	public static void main(String[] args) {
		// System.out.println(new VoTooL().getJSONString(codeMaps));
		System.out.println(getSQLString("getUserInForumByUsername"));
	}
}
