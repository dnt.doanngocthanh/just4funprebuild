package com.jbdc.config;


import java.io.File;
import java.util.Properties;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class ConfigManager {
	private static final Properties m_properties = new Properties();
	public static String path;

	public static String getPath() {
		return path;
	}

	public static void setPath(String path) {
		ConfigManager.path = path;
	}

	public static String getConfigPath() {
		String path = getPath();
		if (path == null)
			path = isWindow() ? "D:\\customconfig" : "/opt/tomcat/customconfig";
		return path;

	}

	public static void loadProperties() {
		String path = getConfigPath();
		File initPath = new File(path);
		m_properties.clear();
		loadDir(initPath);
	}

	public static String getProperty(String key) {
		return getProperty(key, null);
	}

	public static String getProperty(String key, String defaultValue) {
		String encodStr = getProperty().getProperty(key, defaultValue);
		try {
			if (encodStr != null)
				encodStr = new String(encodStr.getBytes("iso-8859-1"));
			if(encodStr ==null|| encodStr.isBlank()||encodStr.isEmpty()) {
				System.err.println("[WARNING]:["+key+"] >>>>>>>>>>>>>>>>>>>>getProperty with NODATA!");
			}
		} catch (UnsupportedEncodingException e) {
			System.out.println(e);

		}
		return encodStr;
	}

	public static Properties getProperty() {
		if (m_properties.size() == 0)
			initLoadProperties();
		return m_properties;
	}

	public static boolean isWindow() {
		return System.getProperty("file.separator").equals("\\");
	}

	private static void loadDir(File dir) {
		File[] files = dir.listFiles();
		if (files == null)
			return;
		for (int i = 0; i < files.length; i++) {
			if (files[i].isDirectory()) {
				loadDir(files[i]);
			} else if (files[i].isFile()) {
				loadFile(files[i]);
			}
		}
	}

	private static void loadFile(File file) {
		if (file == null) {
			System.out.println("loadFile file is null");
			return;
		}
		FileInputStream f = null;
		if (file.getName().toLowerCase().endsWith(".properties"))
			try {
				f = new FileInputStream(file);
				Properties p = new Properties();
				p.load(f);
				m_properties.putAll(p);
			} catch (FileNotFoundException fnfe) {
				System.err.println(file.getAbsoluteFile() + " file not found");
			} catch (IOException ioe) {
				System.err.println(ioe);
			} finally {
				if (f != null)
					try {
						f.close();
					} catch (IOException e) {
						System.err.println(e);
					}
			}
	}

	private static synchronized void initLoadProperties() {
		if (m_properties.size() == 0)
			loadProperties();
	}
}
