package com.JF4.core.quatz;

public class ReturnDataObject {
	private int returnCode;
	private String returnMess;
	private Object returnData;

	public int getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(int returnCode) {
		this.returnCode = returnCode;
	}

	public String getReturnMess() {
		return returnMess;
	}

	public void setReturnMess(String returnMess) {
		this.returnMess = returnMess;
	}

	public Object getReturnData() {
		return returnData;
	}

	public void setReturnData(Object returnData) {
		this.returnData = returnData;
	}

}
