package com.JF4.core.quatz;

import org.quartz.CronScheduleBuilder;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.Trigger.TriggerState;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;

public class SchedulerManager {
	private static Scheduler scheduler;
	static {
		scheduler = SchedulerProvider.getScheduler();
	}

	private static Class<? extends Job> classBathExcute = null;

	public boolean checkSchedule(String jobName) {
		TriggerKey triggerKey = TriggerKey.triggerKey(jobName);
		TriggerState triggerState = null;
		try {
			triggerState = scheduler.getTriggerState(triggerKey);
		} catch (SchedulerException e) {
			return false;
		}
		if (triggerState == TriggerState.NORMAL) {
			return true;
		} else {
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	public void execute(String packageClass, String cron_expression, String dess) {
        if (!checkSchedule(classBathExcute.getSimpleName())) {
			try {
				classBathExcute = (Class<? extends Job>) Class.forName(packageClass);
				JobKey jobKey = new JobKey(classBathExcute.getName());
				try {
					if (!scheduler.checkExists(jobKey)) {
						JobDetail jobDetail = JobBuilder.newJob(classBathExcute).withIdentity(jobKey).build();
						TriggerBuilder<Trigger> triggerBuilder = TriggerBuilder.newTrigger()
								.withIdentity(classBathExcute.getName());
						Trigger trigger = triggerBuilder.withSchedule(CronScheduleBuilder.cronSchedule(cron_expression))
								.build();
						scheduler.scheduleJob(jobDetail, trigger);
					}
				} catch (SchedulerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (ClassNotFoundException e) {
			
				e.printStackTrace();
			}
		}
	}
}
