package com.JF4.core.sevice;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.maven.model.Dependency;

import com.JF4.core.config.mavenConfig;
import com.JF4.core.config.objectConfig;
import com.dano.j4F.jdbc.DataSet;
import com.dano.j4F.sevice.DB_Module;

public class snapMavenToDB extends DB_Module {
	public static String getSourceStr() {
		String projectPath = System.getProperty("user.dir");
		File projectDirectory = new File(projectPath);
		String projectName = projectDirectory.getName();
		System.out.println("Project name: " + projectName);
		return projectName;
	}

	public static void snapToDB() {
		String sql = "INSERT INTO MAVEN_TABLE (GROUP_ID, JSON_DATA, SRC) VALUES( :GROUP_ID, :JSON_DATA, :SRC);";
		List<Dependency> list = mavenConfig.dependencies;
		DataSet ds = getDataSet();
		for (Dependency dependency : list) {
			String groupId = dependency.getGroupId();
			String artifactId = dependency.getArtifactId();

			ds.setField("GROUP_ID", groupId);
			ds.setField("JSON_DATA", new objectConfig().getJSONString(dependency));
			ds.setField("SRC", getSourceStr());
			ds.update(sql);
		}
	}

	public static List<Dependency> getMavenFromDB() {
		String sql = "SELECT JSON_DATA FROM MAVEN_TABLE";
		DataSet ds = getDataSet();
		objectConfig votool = new objectConfig();
		List<Dependency> newList = new ArrayList<Dependency>();
		List<Map<String, Object>> list = ds.searchAndRetrieve(sql);
		for (Map<String, Object> map : list) {

			Map<String, String> map1 = new objectConfig().stringJSONToMap(map.get("json_data").toString());
			Dependency dependencities = new Dependency();
			dependencities.setGroupId(map1.get("groupId"));
			dependencities.setArtifactId(map1.get("artifactId"));
			dependencities.setVersion(map1.get("version"));
			if (map1.get("systemPath") != null) {
				dependencities.setSystemPath(map1.get("systemPath"));
			}
			newList.add(dependencities);
		}

		// List<Dependency> list=
		return newList;
	}

	public static void main(String[] args) {
		System.out.println(new objectConfig().getJSONString(getMavenFromDB()));
	}
}
