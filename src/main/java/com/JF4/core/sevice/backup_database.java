package com.JF4.core.sevice;

import java.io.IOException;

public class backup_database {
	public static String mysqlDumpPath = "/usr/bin/mysqldump"; // Đường dẫn tới lệnh mysqldump
	public static String databaseName = "your_database_name"; // Tên cơ sở dữ liệu cần sao lưu
	public static String username = "your_username"; // Tên người dùng MySQL
	public static String password = "your_password"; // Mật khẩu người dùng MySQL
	public static String backupFilePath = "/path/to/backup.sql"; // Đường dẫn tới file sao lưu

	public static void backup() {
		try {
			ProcessBuilder processBuilder = new ProcessBuilder(mysqlDumpPath, "-u" + username, "-p" + password,
					databaseName, "-r", backupFilePath);

			processBuilder.redirectErrorStream(true);
			Process process = processBuilder.start();

			int exitCode = process.waitFor();
			if (exitCode == 0) {
				System.out.println("Backup completed successfully.");
			} else {
				System.out.println("Backup failed with exit code: " + exitCode);
			}
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
	}

}
