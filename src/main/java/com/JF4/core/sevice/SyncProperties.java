package com.JF4.core.sevice;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ObjectUtils;

import com.JF4.core.config.jsonManager;
import com.JF4.core.config.objectConfig;
import com.dano.j4F.jdbc.DataSet;
import com.dano.j4F.sevice.DB_Module;

public class SyncProperties extends DB_Module {

	private static final String sqlinsert = "INSERT INTO PROPERTIES (`KEY`, `DATA`) VALUES(:KEY,:DATA)";
	private static final String sqlupdate = "UPDATE PROPERTIES SET `DATA`=:DATA WHERE `KEY` =:KEY";

	public static String getSql(Object key) {
		DataSet ds = getDataSet();
		ds.setField("KEY", key);
		List<Map<String, Object>> isHaveData = ds.searchAndRetrieve("SELECT  * FROM  PROPERTIES p WHERE p.KEY = :KEY");
		if (isHaveData.size() >= 1) {
			return sqlupdate;
		}
		return sqlinsert;
	}

	public static boolean syncData() {
		Map map = jsonManager.getProperty();
		ArrayList keyList = new ArrayList(map.keySet());
		try {
			for (Object object : keyList) {
				System.out.println(object);
				String fileName = ObjectUtils.toString(object);
				if (!fileName.endsWith(".json")) {
					if (object != null) {
						DataSet ds = getDataSet();
						ds.setField("KEY", object);
						objectConfig votool = new objectConfig();
						ds.setField("DATA", votool.getJSONString(map.get(object)));
						ds.update(getSql(object));
					}
				}

			}
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public static void main(String[] args) {
		System.out.println(syncData());
	}
}
