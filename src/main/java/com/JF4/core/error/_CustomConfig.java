package com.JF4.core.error;

import java.util.HashMap;
import java.util.Map;

import com.JF4.core.config.jsonManager;
import com.JF4.core.config.objectConfig;

public class _CustomConfig {
	private static Map map = new HashMap();
	private static objectConfig votool;
	static {
		votool = new objectConfig();
	}

	public static Map getProperty(String key) {
		System.out.println(getNameClassConfig(_CustomConfig.class));
		String mapStr = null;
		mapStr = jsonManager.getProperty(getNameClassConfig(_CustomConfig.class));
		System.out.println(mapStr);
		objectConfig votool = new objectConfig();
		if (mapStr != null || mapStr.isEmpty()) {
			return votool.mapStringToMap(mapStr);
		}
		return null;
	}

	public Map getMap(String nameUpdate) {
		return votool.stringJSONToMap(jsonManager.getProperty(nameUpdate));
	}

	public static Map getMapData() {
		_CustomConfig map = new _CustomConfig();
		return map.getMap(getNameClassConfig(_CustomConfig.class));
	}

	public static void put(String key, String value) {
		map.clear();
		map = getMapData();
		map.put(key, value);
		jsonManager.updateProperty(getNameClassConfig(_CustomConfig.class), map);
	}

	public static String getNameClassConfig(Class<?> classInfo) {
		return classInfo.getName();
	}

	public static void main(String[] args) {
		put("123456", "7890");
		System.out.println();
	}
}
