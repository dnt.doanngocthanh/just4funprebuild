package com.JF4.core.error;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.JF4.core.config.jsonManager;
import com.JF4.core.config.objectConfig;

public class MapErrorCode extends Exception {
	private List<Map> listMapErrorCode;
	private objectConfig votool = new objectConfig();
	private String nameUpdate = this.getClass().getName();
	private static Map map = new HashMap();

	public MapErrorCode() {
		listMapErrorCode = new ArrayList<Map>();
	}

	public String getError_(String errorCode) {
		return votool.stringJSONToMap(jsonManager.getProperty(nameUpdate)).get(errorCode).toString();
	}

	public Map getMapErrorCode() {
		return votool.stringJSONToMap(jsonManager.getProperty(nameUpdate));
	}

	public static Map getMapData() {
		MapErrorCode map = new MapErrorCode();
		return map.getMapErrorCode();
	}

	public static void put(String key, String value) {
		map.clear();
		map = getMapData();
		map.put(key, value);
		jsonManager.updateProperty(MapErrorCode.class.getName(), map);
	}

	public static String getError(String errorCode) {
		return new MapErrorCode().getError_(errorCode);
	}

	private static final long serialVersionUID = 1L;

	public MapErrorCode(String message) {
		super(message);
	}

	public static void main(String[] args) {
		put("test2", "123556");
		put("test", "123556");
		put("test1", "123556");
		put("test2", "123222");
		put("test3", "123333");
		System.out.println(map);

	}
}
