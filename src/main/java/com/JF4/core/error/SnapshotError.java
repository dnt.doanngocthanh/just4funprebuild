package com.JF4.core.error;

import java.io.PrintWriter;
import java.io.StringWriter;

public class SnapshotError {

	public static void put(Exception e) {
		String stackTrace = getStackTraceAsString(e);
		System.out.println(e.getMessage());
		System.out.println(stackTrace);
	}

	private static String getStackTraceAsString(Exception e) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		return sw.toString();
	}

	public static void main(String[] args) {
		try {
			int result = 10 / 0;
			System.out.println(result);
		} catch (Exception e) {
			put(e);
		}
	}
}
