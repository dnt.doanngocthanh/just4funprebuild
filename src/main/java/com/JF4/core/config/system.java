package com.JF4.core.config;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class system extends securityConfig {

	private static final String LOG_FILE_PATH = dirConfig.logPath + "log/" + getCurrentDate() + ".txt";

	public static void main(String[] args) {
		// Ghi log
		writeLog("This is a log message.");
		writeLog("This is an error message.", LogLevel.ERROR);
	}

	public static void logNoTime(String message) {
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(LOG_FILE_PATH, true))) {
			writer.write(message);
			writer.newLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void writeLog(String message) {
		writeLog(message, LogLevel.INFO);
	}

	public static void writeLog(String message, LogLevel logLevel) {
		String logText = "[" + getCurrentTimestamp() + "] " + "[" + logLevel + "] " + message;

		try (BufferedWriter writer = new BufferedWriter(new FileWriter(LOG_FILE_PATH, true))) {
			writer.write(logText);
			writer.newLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static String getCurrentDate() {
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		return now.format(formatter);
	}

	private static String getCurrentTimestamp() {
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		return now.format(formatter);
	}

	private enum LogLevel {
		INFO, WARNING, ERROR
	}
}
