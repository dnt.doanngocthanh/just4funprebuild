package com.JF4.core.config;

import java.io.File;
import java.io.FileReader;
import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.maven.model.Dependency;
import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;

public class mavenConfig {
	public static List<Dependency> dependencies;
	private String nextline = "\n";

	static {
		dependencies = snapShotDependencies();
	}

	private static String getDependencyStr(String escapedGroupId, String escapedArtifactId, String escapedVersion) {
		StringBuilder str = new StringBuilder();
		str.append("<dependency>\n");
		str.append("<groupId>\n");
		str.append(escapedGroupId);
		str.append("</groupId>\n");
		str.append("<artifactId>");
		str.append(escapedArtifactId);
		str.append("</artifactId>\n");
		str.append("<version>");
		str.append(escapedVersion);
		str.append("</version>\n");
		str.append("</dependency>");
		return str.toString();
	}

	public static List<Dependency> snapShotDependencies() {
		File pomFile = new File(dirConfig.pathMavenFile);
		try (FileReader reader = new FileReader(pomFile)) {
			MavenXpp3Reader xpp3Reader = new MavenXpp3Reader();
			Model model = xpp3Reader.read(reader);
			dependencies = model.getDependencies();
		} catch (Exception e) {
			e.printStackTrace();
		}
		// System.out.println(new objectConfig().getJSONString(dependencies));
		return dependencies;
	}

	

	public static void main(String[] args) {
		for (Dependency dependency : dependencies) {
			String groupId = dependency.getGroupId();
			String artifactId = dependency.getArtifactId();
			String version = dependency.getVersion();
			String escapedGroupId = StringEscapeUtils.escapeXml10(groupId);
			String escapedArtifactId = StringEscapeUtils.escapeXml10(artifactId);
			String escapedVersion = StringEscapeUtils.escapeXml10(version);

//			String xml = "<dependency>\n" + "  <groupId>" + escapedGroupId + "</groupId>\n" + "  <artifactId>"
//					+ escapedArtifactId + "</artifactId>\n" + "  <version>" + escapedVersion + "</version>\n"
//					+ "</dependency>";
			System.out.println(getDependencyStr(escapedGroupId, escapedArtifactId, escapedVersion));

		}

	}
}
