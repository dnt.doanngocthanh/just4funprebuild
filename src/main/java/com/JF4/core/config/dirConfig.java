package com.JF4.core.config;

import java.io.File;

public class dirConfig {
	public static String path = systemConfig.isWindow() ? "D:\\customconfig" : "/opt/tomcat/customconfig";
	public static String pathMavenFile = PomFilePath();
	public static String logPath = systemConfig.isWindow() ? "D:\\" : "/opt/tomcat/log";
	public static String pathLibrary;
	public static String pathTomcatSever;
	public static String pathUpload;
	public static String pathSource;
	public static String urlGetConfig = "https://webdano.com/api/getconfigdata";
	private static final String filePomName = "pom.xml";
	static {

		// System.out.println(configManager.getProperty("com.dano.j4F.batch.batchBean.getFireBaseConfig"));
	}

	public static String getPath() {
		return path;
	}

	public static void setPath(String path) {
		dirConfig.path = path;
	}

	public static String getPathMavenFile() {
		return pathMavenFile;
	}

	public static void setPathMavenFile(String pathMavenFile) {
		dirConfig.pathMavenFile = pathMavenFile;
	}

	public static String getPathLibrary() {
		return pathLibrary;
	}

	public static void setPathLibrary(String pathLibrary) {
		dirConfig.pathLibrary = pathLibrary;
	}

	public static String getPathTomcatSever() {
		return pathTomcatSever;
	}

	public static void setPathTomcatSever(String pathTomcatSever) {
		dirConfig.pathTomcatSever = pathTomcatSever;
	}

	public static String getPathUpload() {
		return pathUpload;
	}

	public static void setPathUpload(String pathUpload) {
		dirConfig.pathUpload = pathUpload;
	}

	public static String getPathSource() {
		return pathSource;
	}

	public static void setPathSource(String pathSource) {
		dirConfig.pathSource = pathSource;
	}

	public static String PomFilePath() {
		return new File(filePomName).getAbsolutePath();
	}

	public static void main(String[] args) {
		System.out.println("á " + path);
		System.out.println(pathMavenFile);

	}
//	ClassLoader classLoader = dirConfig.class.getClassLoader();
//	URL pomUrl = classLoader.getResource("pom.xml");
//
//	if (pomUrl != null) {
//		String pomFilePath = pomUrl.getPath();
//		System.out.println("POM file path: " + pomFilePath);
//		return pomFilePath;
//
//	} else {
//		System.out.println("POM file not found");
//	}
}
