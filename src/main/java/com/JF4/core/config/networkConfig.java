package com.JF4.core.config;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.JF4.core.sevice.http;

public class networkConfig {
	public static String macAddress;
	public static String ipPublic;
	public static String siffixDns;
	public static String ipPrivate;
	static {
		init();
	}

	public static void init() {
		http api = new http();
		String command = "ifconfig -a";
		if (systemConfig.isWindow()) {
			command = "ipconfig /all";
		}
		try {
			Process process = Runtime.getRuntime().exec(command);
			BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
			String line;
			while ((line = reader.readLine()) != null) {

				if (line.contains("Primary Dns Suffix") || line.contains("ether")) {
					siffixDns = line.substring(line.indexOf(":") + 1).trim();

				}
				if (line.contains("Physical Address") || line.contains("ether")) {
					macAddress = line.substring(line.indexOf(":") + 1).trim();
					break;
				}
				if (line.contains("inet addr:") || line.contains("inet ")) {
					String ipAddressPattern = "(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3})";
					Pattern pattern = Pattern.compile(ipAddressPattern);
					Matcher matcher = pattern.matcher(line);

					if (matcher.find()) {
						ipPrivate = matcher.group(1);
						break;
					}
				}
			}
			api.offLog();
			api.call("https://api.ipify.org");
			ipPublic = api.getStringData();
			System.out.println("===================================================");
			System.out.println("macAddress:" + macAddress);
			System.out.println("ipPublic:" + ipPublic);
			System.out.println("siffixDns:" + siffixDns);
			System.out.println("ipPrivate:" + ipPrivate);
			System.out.println("===================================================");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		System.out.println(macAddress);
		System.out.println(siffixDns);
		System.out.println(ipPrivate);
	}
}
