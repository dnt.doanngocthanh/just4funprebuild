package com.JF4.core.config;

import java.util.Random;
import java.util.UUID;
import java.security.SecureRandom;

public class ID {
	private static final String CHARSET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	private static final int keyLength = 24;

	public static synchronized String UUID() {
		return UUID.randomUUID().toString();
	}

	public static synchronized String Number(int length) {
		Random rm = new Random(System.currentTimeMillis());
		StringBuffer str = new StringBuffer();
		int randomBound = 10;
		for (int i = 0; i < length; i++) {
			str.append(rm.nextInt(randomBound));
		}
		return str.toString();
	}

	public static String generateRandomKey(int length) {
		SecureRandom random = new SecureRandom();
		StringBuilder keyBuilder = new StringBuilder();

		for (int i = 0; i < length; i++) {
			int randomIndex = random.nextInt(CHARSET.length());
			char randomChar = CHARSET.charAt(randomIndex);
			keyBuilder.append(randomChar);
		}

		return keyBuilder.toString();
	}

	public static String shareKey() {
		return generateRandomKey(keyLength);

	}

	public static void main(String[] args) {
		System.out.println(Number(5));
		System.out.println(shareKey());
		System.out.println(shareKey());
		System.out.println(UUID());
		System.out.println(UUID());
		System.out.println(UUID());
		System.out.println(UUID());
	}
}
