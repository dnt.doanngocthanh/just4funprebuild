package com.JF4.core.config;

import com.JF4.core.error.MapErrorCode;

public abstract class securityConfig {
	protected boolean is_active;
	protected static final String macAddress = networkConfig.macAddress;
	static {
		if (!checkMacAddress()) {
			exit(MapErrorCode.getError("securityConfig.macAddress.notMacAddress"));
		}
	}

	protected static boolean checkMacAddress() {
		if (macAddress == null || macAddress.isEmpty()) {
			exit(MapErrorCode.getError("securityConfig.macAddress.notKey"));
		}
		return true;
	}

	protected static void exit(String messenger) {
		System.err.println(messenger.toUpperCase());
		System.exit(0);
	}

	protected static void resignNewKeyByMac() {
		System.out.println(macAddress);
	}

	public static void main(String[] args) {
		checkMacAddress();
	}
}
