package com.JF4.core.config;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.ObjectUtils;

import com.JF4.core.sevice.http;

public class getApiConfigManager {
	private static final Map<String, Map> m_jsonObject = new HashMap<String, Map>();
	public static Map<String, Map> getProperty() {
		if (m_jsonObject.size() == 0)
			initLoadProperties();
		return m_jsonObject;
	}

	private static synchronized void initLoadProperties() {
		if (m_jsonObject.size() == 0)
			loadJSON();

	}

	public static void loadJSON() {
		m_jsonObject.clear();
		http api = new http();
		api.offLog();
		objectConfig votool = new objectConfig();
		try {
			api.call(
					"https://sbcs.cathaylife.com.vn/ZSWeb/api-gate/getMainContract?CUSTOMER_NUMBER=0000026172&LANGUAGE=vi_VN");
			Map propertiesFromFile = votool.stringJSONToMap(api.getStringData());
			m_jsonObject.putAll(propertiesFromFile);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static String getProperty(String key, String defaultValue) {
		Map map = getProperty();
		String encodStr = null;
		try {
			encodStr = ObjectUtils.toString(map.get(key));
			if (encodStr != null)

				encodStr = new String(encodStr.getBytes("utf-8"));
			if (encodStr == null || encodStr.equals("") || encodStr.isEmpty()) {
				System.err.println("[WARNING]:[" + key + "] >>>>>>>>>>>>>>>>>>>>getProperty with NODATA!");
			}
		} catch (UnsupportedEncodingException e) {
			System.err.println("[ERROR]:[" + key + "] >>>>>>>>>>>>>>>>>>>>" + e.getMessage());

		}
		return encodStr;
	}

	public static String getProperty(String key) {
		return getProperty(key, null);
	}

	public static void main(String[] args) {
		objectConfig votool = new objectConfig();

		System.out.println(votool.mapStringToMap(getProperty("returnData")));
	}
}
