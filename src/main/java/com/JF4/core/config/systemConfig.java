package com.JF4.core.config;

public class systemConfig {
	public static boolean isWindow() {
		return System.getProperty("file.separator").equals("\\");
	}

	public static boolean isRunningAsWebApplication() {// running with server.
		String serverInfo = System.getProperty("catalina.base");
		return serverInfo != null;
	}

	public static void main(String[] args) {
		System.out.println(isRunningAsWebApplication());
	}
}
