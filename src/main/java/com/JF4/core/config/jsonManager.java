package com.JF4.core.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.ObjectUtils;

import com.JF4.core.sevice.SyncProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class jsonManager {
	public static String path = dirConfig.path;
	public static final Map<String, Map> m_jsonObject = new HashMap<String, Map>();

	public static boolean isWindow() {
		return System.getProperty("file.separator").equals("\\");
	}

	public static void loadJSON() {
		File initPath = new File(path);
		m_jsonObject.clear();
		loadDir(initPath);
	}

	private static void loadDir(File dir) {
		File[] files = dir.listFiles();
		if (files == null)
			return;
		for (int i = 0; i < files.length; i++) {
			if (files[i].isDirectory()) {
				loadDir(files[i]);
			} else if (files[i].isFile()) {
				loadFile(files[i]);
			}
		}
	}

	public static void loadFile(File file) {
		if (file == null) {
			System.out.println("loadFile file is null");
			return;
		}
		FileInputStream f = null;
		if (file.getName().toLowerCase().endsWith(".json"))
			try {
				f = new FileInputStream(file);

				ObjectMapper objectMapper = new ObjectMapper();

				Map propertiesFromFile = objectMapper.readValue(f, Map.class);

				propertiesFromFile.put(file.getName(), propertiesFromFile);
				m_jsonObject.putAll(propertiesFromFile);
			} catch (FileNotFoundException fnfe) {
				System.err.println(file.getAbsoluteFile() + " file not found");
			} catch (IOException ioe) {
				System.err.println(ioe);
			} finally {
				if (f != null)
					try {
						f.close();
					} catch (IOException e) {
						System.err.println(e);
					}
			}
	}

	public static String getProperty(String key) {
		return getProperty(key, null);
	}

	public static String getProperty(String key, String defaultValue) {
		Map map = getProperty();
		String encodStr = null;
		try {
			objectConfig votool = new objectConfig();
			String dataReturn = ObjectUtils.toString(map.get(key));

			if (dataReturn.startsWith("[") || dataReturn.startsWith("{")) {
				encodStr = ObjectUtils.toString(votool.getJSONString(map.get(key)));
			} else {
				encodStr = dataReturn;
			}

			if (encodStr != null)

				encodStr = new String(encodStr.getBytes("utf-8"));
			if (encodStr == null || encodStr.equals("") || encodStr.isEmpty()) {
				System.err.println("[WARNING]:[" + key + "] >>>>>>>>>>>>>>>>>>>>getProperty with NODATA!");
			}
		} catch (UnsupportedEncodingException e) {
			System.err.println("[ERROR]:[" + key + "] >>>>>>>>>>>>>>>>>>>>" + e.getMessage());

		}
		return encodStr;
	}

	public static Map<String, Map> getProperty() {
		if (m_jsonObject.size() == 0)
			initLoadProperties();
		return m_jsonObject;
	}

	private static synchronized void initLoadProperties() {
		if (m_jsonObject.size() == 0)
			loadJSON();

	}

	public static synchronized void updateProperty(String key, Object value) {
		File initFile = readJsonFile(key, value);
		Map propertiesFromFile = new HashMap();
		if (initFile != null) {
			propertiesFromFile.putAll(getMapFromFile(initFile));
			propertiesFromFile.put(key, value);
			writeJsonFile(initFile.getPath(), propertiesFromFile);
		} else {
			initFile = new File(path + "/systemFile.json");
			propertiesFromFile.putAll(getMapFromFile(initFile));
			System.out.println(initFile.getName());
			propertiesFromFile.put(key, value);
			writeJsonFile(initFile.getPath(), propertiesFromFile);
		}
		
		 if (!systemConfig.isWindow()) { SyncProperties.syncData(); }
		 
	}

	private static Map getMapFromFile(File file) {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			return objectMapper.readValue(file, Map.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	private static File readJsonFile(String key, Object value) {
		File initPath = new File(path);
		File[] files = initPath.listFiles();
		for (int i = 0; i < files.length; i++) {
			if (files[i].isDirectory()) {
				return readJsonFile(files[i].getAbsolutePath(), key);
			} else if (files[i].isFile()) {
				if (files[i].getName().toLowerCase().endsWith(".json")) {
					ObjectMapper objectMapper = new ObjectMapper();
					Map propertiesFromFile = getMapFromFile(files[i]);
					if (propertiesFromFile.get(key) != null) {
						System.out.println(files[i].getName());
						return files[i];
					}

				}

			}
		}
		return null;
	}

	private static void writeJsonFile(String filePath, Map<String, Object> jsonData) {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
			File file = new File(filePath);
			objectMapper.writeValue(file, jsonData);
			System.out.println("JSON file updated successfully.");
		} catch (IOException e) {
			System.err.println("Error writing JSON file: " + e.getMessage());
		}
	}

	public static void main(String[] args) {
		Map map = new HashMap();
		map.put("securityConfig.macAddress.notMacAddress", "Không tìm thấy địa chỉ MAC");
		map.put("securityConfig.macAddress.notKey", "Khoong tim thay keyyyy");
		updateProperty("com.JF4.core.error.MapErrorCode", map);
	}
}
