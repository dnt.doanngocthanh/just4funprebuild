package com.JF4.core.security;

import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class PKCS5_Security {
	private static final String algorithm = "DESede/ECB/PKCS5Padding";

	public static String base64Encode_PKCS5Padding(String jsonData) throws Exception {
		byte[] keyBytes = "qgzndruubvgfulmzkjukjwot".getBytes("UTF-8");

		SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "DESede");
		Cipher cipher = Cipher.getInstance(algorithm); // DESede/ECB/PKCS5Padding
		cipher.init(Cipher.ENCRYPT_MODE, keySpec);
		byte[] encryptedBytes = cipher.doFinal(jsonData.getBytes("UTF-8"));
		String encryptedData = Base64.getEncoder().encodeToString(encryptedBytes);

		System.out.println("Encrypted data: " + encryptedData);
		return encryptedData;
	}

	public static String decodeBase64_PKCS5Padding(String encryptedData) throws Exception {
		byte[] keyBytes = "ấdada".getBytes("UTF-8");
		SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "DESede");
		Cipher cipher = Cipher.getInstance(algorithm);
		cipher.init(Cipher.DECRYPT_MODE, keySpec);
		byte[] decodedData = Base64.getDecoder().decode(encryptedData);
		byte[] decryptedBytes = cipher.doFinal(decodedData);
		String decryptedData = new String(decryptedBytes, "UTF-8");
		System.out.println(decryptedData);
		return decryptedData;
	}

	public static void main(String[] args) {
		try {

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
