package com.JF4.core.security;

import java.io.File;
import java.io.FileInputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.NoSuchFileException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import org.codehaus.plexus.util.Base64;

/**
 * SHA-256 is a perfectly good secure hashing algorithm and quite suitable for
 * use on certificates while 2048-bit RSA is a good signing algorithm (do note
 * that signing is not the same as encrypting).
 * 
 * @since 2021-07-25
 * 
 * @author 0100506536 KeyStore Explore 5.4.4 using to create KeyPair (PublicKey
 *         - PrivateKey)
 * 
 *         {@docRoot SHA256withRSA_howto_generation.pptx}
 * 
 * @version 1.0
 * 
 **/

public class SHA256withRSA {
	PrivateKey PRIVATE_KEY = null;
	PublicKey PUBLIC_KEY = null;
	private static final String CRYPTO_METHOD = "RSA";
	private static final String SIGNATURE_ALGORITHM = "SHA256withRSA";
	private final static int CRYPTO_BITS = 2048;

	/**
	 * GET private key
	 * 
	 * @return PrivateKey PRIVATE_KEY
	 */
	public PrivateKey getPRIVATE_KEY() {
		return PRIVATE_KEY;
	}

	/**
	 * GET private key
	 * 
	 * @return String
	 * @throws Exception
	 */
	public String getPRIVATE_KEY_AsString() throws Exception {
		if (PRIVATE_KEY == null) {
			throw new Exception("Value of PRIVATE_KEY is null, cannot generate to string format");
		}

		RSAPrivateKey privateKey = (RSAPrivateKey) this.getPRIVATE_KEY();

		return new String(Base64.encodeBase64(privateKey.getEncoded()));
	}

	/**
	 * GET private key
	 * 
	 * @throws Exception
	 */
	public void getPRIVATE_KEY_FromFile(String privateKeyFilePath) throws Exception {
		StringBuffer sb = new StringBuffer();
		File privateFile = new File(privateKeyFilePath);
		if (!privateFile.exists()) {
			throw new NoSuchFileException("File not exists, please check again...!");
		}

		try {
			if (privateKeyFilePath.endsWith(".pkcs8")) {
				// PKCS8
				FileInputStream fis = new FileInputStream(privateFile);
				int i = 0;
				while ((i = fis.read()) != -1) {
					sb.append((char) i);
				}

				fis.close();

				String fileContent = sb.toString();
				fileContent = fileContent.replaceAll("-----BEGIN RSA PRIVATE KEY-----", "")
						.replaceAll("-----END RSA PRIVATE KEY-----", "").replaceAll("\n", "");

				this.setPRIVATE_KEY(fileContent);
				System.out.println("set PRIVATE KEY OK");
			} else if (privateKeyFilePath.endsWith(".key")) {
				// OpenSSL
				FileInputStream fis = new FileInputStream(privateFile);
				int i = 0;
				while ((i = fis.read()) != -1) {
					sb.append((char) i);
				}

				fis.close();

				String fileContent = sb.toString();
				fileContent = fileContent.replaceAll("-----BEGIN PRIVATE KEY-----", "")
						.replaceAll("-----END PRIVATE KEY-----", "").replaceAll("\n", "");

				this.setPRIVATE_KEY(fileContent);
			} else {
				throw new UnsupportedOperationException("Only supported PKCS8 file or OpenSSL file");
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Import private key from file unsuccessfully");
		} finally {
			sb.setLength(0);
		}

	}

	/**
	 * SET private key
	 */
	public void setPRIVATE_KEY(PrivateKey newPrivateKey) {
		PRIVATE_KEY = newPrivateKey;
	}

	/**
	 * SET private key
	 * 
	 * @throws Exception
	 */
	public void setPRIVATE_KEY(String newPrivateKey) throws Exception {
		// Supported PKCS8
		try {
			newPrivateKey = newPrivateKey.replaceAll("-----BEGIN PRIVATE KEY-----", "")
					.replaceAll("-----END PRIVATE KEY-----", "").replaceAll("\n", "").replaceAll("\\s+", "");
			byte[] encodedBytes = Base64.encodeBase64(newPrivateKey.getBytes());
			System.out.println(encodedBytes.length);
			PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(encodedBytes);
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			PrivateKey privateKey = keyFactory.generatePrivate(keySpec);
			this.setPRIVATE_KEY(privateKey);
			System.out.println("set PRIVATE KEY");
		} catch (InvalidKeySpecException ie) {
			throw new Exception("Parse error, your input stirng maybe not PKCS8 format");
		} catch (Exception e) {
			throw new Exception("Unable to convert from string to private key RSA");
		}
	}

	/**
	 * GET public key
	 */
	public PublicKey getPUBLIC_KEY() {
		return PUBLIC_KEY;
	}

	/**
	 * GET public key
	 * 
	 * @throws Exception
	 */
	public String getPUBLIC_KEY_AsString() throws Exception {
		if (PUBLIC_KEY == null) {
			throw new Exception("Value of PUBLIC_KEY is null, cannot generate to string format");
		}

		RSAPublicKey publicKey = (RSAPublicKey) this.getPUBLIC_KEY();

		return new String(Base64.encodeBase64(publicKey.getEncoded()));
	}

	/**
	 * GET public key
	 * 
	 * @throws Exception
	 */
	public void getPUBLIC_KEY_FromFile(String publicKeyFilePath) throws Exception {
		StringBuffer sb = new StringBuffer();
		try {

			File publicKeyFile = new File(publicKeyFilePath);
			if (!publicKeyFile.exists()) {
				throw new NoSuchFileException("File not exists, please check again...!");
			}

			FileInputStream fis = new FileInputStream(publicKeyFile);
			int i = 0;
			while ((i = fis.read()) != -1) {
				sb.append((char) i);
			}

			fis.close();
			String fileContent = sb.toString();
			fileContent = fileContent.replaceAll("-----BEGIN PUBLIC KEY-----", "")
					.replaceAll("-----END PUBLIC KEY-----", "").replaceAll("\n", "");

			this.setPUBLIC_KEY(fileContent);
		} catch (Exception e) {
			throw new Exception("Import public key from file unsuccessfully");
		} finally {
			sb.setLength(0);
		}
	}

	/**
	 * SET public key
	 */
	public void setPUBLIC_KEY(PublicKey newPublicKey) {
		PUBLIC_KEY = newPublicKey;
	}

	/**
	 * SET public key
	 * 
	 * @throws Exception
	 * @param String newPublicKey
	 */
	public void setPUBLIC_KEY(String newPublicKey) throws Exception {

		try {
			newPublicKey = newPublicKey.replaceAll("-----BEGIN PUBLIC KEY-----", "")
					.replaceAll("-----END PUBLIC KEY-----", "").replaceAll("\n", "").replaceAll("\\s+", "");
			byte[] encodedBytes = Base64.decodeBase64(newPublicKey.getBytes());
			System.out.println(encodedBytes.length);
			X509EncodedKeySpec keySpec = new X509EncodedKeySpec(encodedBytes);
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			PublicKey publicKey = keyFactory.generatePublic(keySpec);
			this.setPUBLIC_KEY(publicKey);
		} catch (InvalidKeySpecException ie) {
			throw new Exception("Parse error, your input stirng maybe invalid format");
		} catch (Exception e) {
			throw new Exception("Unable to convert from string to public key RSA");
		}
	}

	/**
	 * Store 2 files Includes public key file(1) and private key file(1)
	 * 
	 * @throws Exception
	 * @param String exportFilesPath
	 */
	/*
	 * public void exportToFiles(String exportFilesPath) throws Exception {
	 * if(PUBLIC_KEY == null) { throw new
	 * Exception("Public key not found, cannot export file"); }
	 * 
	 * if(PRIVATE_KEY == null) { throw new
	 * Exception("Private key not found, cannot export file"); }
	 * 
	 * if(StringUtils.isEmpty(exportFilesPath)) { throw new
	 * Exception("Not found place to export files"); }
	 * 
	 * try { Timestamp currentTimeStamp = new Timestamp(System.currentTimeMillis());
	 * String exportTimeString = currentTimeStamp.toString(); exportTimeString =
	 * exportTimeString.replaceAll(":", "") .replaceAll("-", "") .replaceAll(" ",
	 * "") .replaceAll(".", "");
	 * 
	 * String publicKeyFileName = "PublicKeyFile_" + exportTimeString + ".key";
	 * String privateKeyFileName = "PrivateKeyFile_" + exportTimeString + ".key";
	 * 
	 * File publicKeyFile = new File(exportFilesPath + publicKeyFileName); File
	 * privateKeyFile = new File(exportFilesPath + privateKeyFileName);
	 * 
	 * BufferedWriter publicKeyWriter = new BufferedWriter(new
	 * FileWriter(publicKeyFile)); publicKeyWriter.write(new
	 * String(Base64.encodeBytes(this.getPUBLIC_KEY().getEncoded(),
	 * Base64.DO_BREAK_LINES)));
	 * 
	 * publicKeyWriter.close();
	 * 
	 * BufferedWriter privateKeyWriter = new BufferedWriter(new
	 * FileWriter(privateKeyFile)); privateKeyWriter.write(new
	 * String(Base64.encodeBytes(this.getPRIVATE_KEY().getEncoded(),
	 * Base64.DO_BREAK_LINES)));
	 * 
	 * privateKeyWriter.close(); } catch (Exception e) { throw new
	 * Exception("Export keys pair to files unsuccessfully"); }
	 * 
	 * }
	 */

	/**
	 * Generate key pair dynamic
	 * 
	 * After generate should be store as files to using
	 * 
	 * @throws Exception
	 */
	public void generateKeyPair() throws Exception {

		try {

			KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(CRYPTO_METHOD);
			keyPairGenerator.initialize(CRYPTO_BITS);// 2048 bits default
			KeyPair keyPair = keyPairGenerator.genKeyPair();
			PublicKey publicKey = keyPair.getPublic();
			this.setPUBLIC_KEY(publicKey);

			PrivateKey privateKey = keyPair.getPrivate();
			this.setPRIVATE_KEY(privateKey);
		} catch (NoSuchAlgorithmException e) {
			throw new Exception("Unable generate key pair, algorithm maybe incorrect");
		} catch (Exception ex) {
			throw new Exception("Unable generate key pair, something wrong here");
		}

	}

	/**
	 * Sign request
	 * 
	 * @throws Exception
	 * @param String plainText
	 */
	/*
	 * public String sign(String plainText) throws Exception {
	 * 
	 * if (PRIVATE_KEY == null) { throw new
	 * Exception("Private key not found, cannot sign anything"); }
	 * 
	 * if (plainText.isEmpty()) { throw new Exception("Nothing to sign here"); }
	 * 
	 * try {
	 * 
	 * Signature digitalSignature = Signature.getInstance(SIGNATURE_ALGORITHM);
	 * PrivateKey privateKey = this.getPRIVATE_KEY();
	 * digitalSignature.initSign(privateKey);
	 * digitalSignature.update(plainText.getBytes("UTF-8"));
	 * 
	 * byte[] signed = digitalSignature.sign();
	 * 
	 * return DatatypeConverter.printBase64Binary(signed); } catch (Exception e) {
	 * throw new Exception("Unable signed, SHA256withRSA algorithm..."); }
	 * 
	 * }
	 */
	public String sign(String plainText) throws Exception {
		if (PRIVATE_KEY == null) {
			throw new Exception("Private key not found, cannot sign anything");
		}

		if (plainText.isEmpty()) {
			throw new Exception("Nothing to sign here");
		}

		try {
			Signature digitalSignature = Signature.getInstance(SIGNATURE_ALGORITHM);
			digitalSignature.initSign(PRIVATE_KEY);
			digitalSignature.update(plainText.getBytes(StandardCharsets.UTF_8));

			byte[] signed = digitalSignature.sign();

			return new String(Base64.encodeBase64(signed));
		} catch (Exception e) {
			throw new Exception("Unable to sign using SHA256withRSA algorithm");
		}
	}

	/**
	 * Check response
	 * 
	 * @throws Exception
	 * @param String plainText
	 * @param String signatureToBeVerified - 3rd party signature
	 */
	public boolean verify(String plainText, String signatureToBeVerified) throws Exception {

		if (PUBLIC_KEY == null) {
			throw new Exception("Public key not found, cannot sign anything");
		}

		if (plainText.isEmpty()) {
			throw new Exception("You must enter something to reference");
		}

		if (signatureToBeVerified.isEmpty()) {
			throw new Exception("Nothing to verify here");
		}

		try {

			Signature digitalSignature = Signature.getInstance(SIGNATURE_ALGORITHM);
			PublicKey publicKey = this.getPUBLIC_KEY();
			digitalSignature.initVerify(publicKey);
			digitalSignature.update(plainText.getBytes("UTF-8"));

			return digitalSignature.verify(Base64.decodeBase64(signatureToBeVerified.getBytes()));
		} catch (Exception e) {
			return false;
		}

	}

	/**
	 * Check response
	 * 
	 * @throws Exception
	 * @param String    plainText
	 * @param String    signatureToBeVerified - 3rd party signature
	 * @param PublicKey partyPublicKey - 3rd Public Key
	 */
	public boolean verify(String plainText, String signatureToBeVerified, PublicKey partyPublicKey) throws Exception {

		if (partyPublicKey == null) {
			throw new Exception("Public key not found, cannot sign anything");
		}

		if (plainText.isEmpty()) {
			throw new Exception("You must enter something to reference");
		}

		if (signatureToBeVerified.isEmpty()) {
			throw new Exception("Nothing to verify here");
		}

		try {

			Signature digitalSignature = Signature.getInstance(SIGNATURE_ALGORITHM);
			PublicKey publicKey = partyPublicKey;
			digitalSignature.initVerify(publicKey);
			digitalSignature.update(plainText.getBytes("UTF-8"));

			return digitalSignature.verify(Base64.decodeBase64(signatureToBeVerified.getBytes()));
		} catch (Exception e) {
			return false;
		}

	}
}
