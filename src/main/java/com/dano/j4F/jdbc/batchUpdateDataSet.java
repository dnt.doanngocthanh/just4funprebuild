package com.dano.j4F.jdbc;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.sql2o.Connection;
import org.sql2o.Query;
import org.sql2o.Sql2o;
import org.sql2o.Sql2oException;

import com.dano.j4F.config.Enviroment;

public class batchUpdateDataSet {
	private static int BATCH_SIZE = 0;
	private static int BATCH_COUNT;
	private static Sql2o batchSql2o;
	public Query query;
	private static Connection connection;
	private static String SYS_ID;
	private static String BATCH_NAME;
	private static String GROUP_BATCH;
	private static String EROR;
	private static String STATUS;
	private static boolean notify;
	private static boolean autoCommit;
	private static Map mapSetField = new HashMap();

	public static void clear() {
		mapSetField.clear();
	}

	public Query setField(String key, Object value) {
		query.addParameter(key, value);
		return query;
	}

	static {
		// batchSql2o.open();
		notify = false;
		autoCommit = false;
		BATCH_COUNT = 0;
	}

	public batchUpdateDataSet() {

	}

	public batchUpdateDataSet(String url, String username, String password) {
		this.batchSql2o = new Sql2o(url, username, password);
		this.batchSql2o.open();
	}

	public void setUpBatchInfo(Class<?> clazz) {
		this.SYS_ID = clazz.getSimpleName();
		this.BATCH_NAME = clazz.getSimpleName();
		this.GROUP_BATCH = clazz.getName();
	}

	public void setNotify(boolean notify) {
		this.notify = notify;
	}

	public Connection beginTransaction() {
		connection = batchSql2o.beginTransaction();
		System.out.println("beginTransaction");
		return connection;
	}

	public Connection rollbackTransaction() {
		connection.rollback();
		System.out.println("rollbackTransaction");
		return connection;
	}

	public Connection endTransaction() {
		if (connection != null) {
			connection.close();
			System.out.println("endTransaction");
		}
		if (query != null) {
			query.close();
		}
		return connection;
	}

	public static void setBATCH_SIZE(int bATCH_SIZE, int dataSize) {
		BATCH_SIZE = bATCH_SIZE;
		int batchCount = (int) Math.ceil((double) dataSize / BATCH_SIZE);
		BATCH_COUNT = batchCount;
	}

	public Query addBatch() {
		return query.addToBatch();
	}

	public Query preparedBatch(String sql) {
		this.query = connection.createQuery(sql);
		return query;
	}

	public Sql2o commit() {
		return connection.commit();
	}

	@SuppressWarnings("finally")
	public int[] executeBatch(boolean autoComit) throws SQLException {
		int[] result = null;
		boolean is_err = false;
		try {
			this.query.getConnection().getJdbcConnection().setAutoCommit(autoComit);
			result = this.query.executeBatch().getBatchResult().clone();

		} catch (Sql2oException e) {
			is_err = true;
			connection.rollback();
			System.err.println("EROR :" + e.getMessage());
		} finally {
			if (!autoComit) {
				if (is_err) {
					connection.close();
					if (BATCH_NAME != null) {
						STATUS = "【FAILED!】 ";
						Enviroment.notyfiDefault.sendMessage("[" + BATCH_NAME + "] " + STATUS);
					}

				} else {
					System.err.println("NONE ERR");
					connection.commit();
				}
			} else {
				connection.close();
			}
		}
		return result;
	}

	public Query setQuery(Map map) {
		ArrayList<String> myKeyList = new ArrayList<>(map.keySet());
		Map<String, List<Integer>> mapCheckItem = query.getParamNameToIdxMap();
		List<String> missingParams = new ArrayList<>();

		for (String paramName : mapCheckItem.keySet()) {
			if (!map.containsKey(paramName)) {
				missingParams.add(paramName);
			}
		}

		if (!missingParams.isEmpty()) {
			StringBuilder errorMessage = new StringBuilder("MISSING PARAM: [:");
			for (String missingParam : missingParams) {
				errorMessage.append(missingParam + "]").append(", ");
			}
			errorMessage.delete(errorMessage.length() - 2, errorMessage.length());
			System.out.println(errorMessage.toString());

			throw new IllegalArgumentException(errorMessage.toString());
		}

		for (String key : myKeyList) {
			Object value = (Object) map.get(key);
			try {
				query.addParameter(key, value);
				BATCH_COUNT++;
				System.out.println("ADD_TO_BATCH [" + key + "=" + value + "]");
			} catch (Exception e) {
				if (e.getCause() != null && !e.getCause().equals("null")) {
					System.out.println("SET PARAM ERR [" + key + "=" + value + "]");
					System.out.println("=>>>>>>" + e.getCause());

					throw new Sql2oException(e);
				}
			}
		}

		return query;
	}

	public Connection execute() {
		return query.executeBatch();
	}

	public void setAutoCommit(boolean autoCommit) {
		this.autoCommit = autoCommit;
	}

	public int getBatchMaxAdd() {
		return (int) Math.ceil((double) BATCH_COUNT / BATCH_SIZE);
	}

	public int[] executeBatch() {
		int[] checkStatus = null;
		try {

			connection = query.executeBatch();
			checkStatus = connection.getBatchResult();
			if (!this.autoCommit) {
				connection.commit();
			}
			this.EROR = "";
			this.STATUS = "【COMPLETE!】 ";
			checkStatus[0] = -1;
		} catch (Sql2oException e) {
			this.EROR = e.getMessage();
			this.STATUS = "【FAILED!】 ";

			if (!this.autoCommit) {
				connection.rollback();
			}

			if (e.getMessage().contains("Error while executing batch operation: Duplicate entry '")) {
				checkStatus[0] = 1;
			}
		}
		if (notify) {
			if (!autoCommit) {
				String mess = "[" + BATCH_NAME + "] " + STATUS + " " + EROR;
				Enviroment.notyfiDefault.sendMessage(mess);
			}
		} else {
			System.out.println("[" + BATCH_NAME + "] " + STATUS + " " + EROR);
			if (notify) {
				Enviroment.notyfiDefault.sendMessage("[" + BATCH_NAME + "] " + STATUS + " " + EROR);
			}
		}
		return checkStatus;

	}

	public static void main(String[] args) {
	}

}
