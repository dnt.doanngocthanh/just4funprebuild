package com.dano.j4F.config;

import com.dano.j4F.chatbot.notify_telegram;
import com.dano.j4F.sevice.ConfigManager;

public abstract class Enviroment {
	public static String version = "1.0";
	public static String forDriver = "mysql";
	public static notify_telegram notyfiDefault;
	public static String encode = "UTF-8";

	public static boolean isWindow() {
		return System.getProperty("file.separator").equals("\\");
	}

	static {
		notyfiDefault = new notify_telegram();
		notyfiDefault.setBOT_TOKEN(ConfigManager.getProperty("com.dano.j4F.chatbot.notify_telegram.stag1.token"));
		notyfiDefault.setBOT_USERNAME(ConfigManager.getProperty("com.dano.j4F.chatbot.notify_telegram.stag1.username"));
		notyfiDefault.setCHAT_ID(ConfigManager.getProperty("com.dano.j4F.chatbot.notify_telegram.stag1.chatID"));
	}
}
