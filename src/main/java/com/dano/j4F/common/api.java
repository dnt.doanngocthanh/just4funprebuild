package com.dano.j4F.common;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.dano.j4F.plugin.API_HTTP;
import com.dano.j4F.plugin.CallMod;
import com.dano.j4F.plugin.returnObject;

public class api {
	private static Exception exception;

	public static Exception getException() {
		return exception;
	}

	public static void setException(Exception exception) {
		api.exception = exception;
	}

	public static List<Map<String, Object>> call(String method, String url_api) {
		API_HTTP api_http = new API_HTTP();
		api_http.setMethod(method);
		api_http.offLog();
		try {
			List<Map<String, Object>> li = api_http.callApi(url_api);
			return li;
		} catch (Exception e) {
			System.err.println(e.getMessage());
			setException(e);
		}
		return null;
	}

	public static List<Map<String, Object>> GET(String url) {
		API_HTTP api_http = new API_HTTP();
		api_http.setMethod("GET");
		api_http.offLog();
		try {
			List<Map<String, Object>> li = api_http.callApi(url);
			return li;
		} catch (Exception e) {
			System.err.println(e.getMessage());
			setException(e);
			e.printStackTrace();

		}
		return null;
	}

	public static String getString(String url) {
		API_HTTP api_http = new API_HTTP();
		api_http.setMethod("GET");
		api_http.offLog();
		try {
			api_http.callApi(url);
			return api_http.getStringData();
		} catch (Exception e) {
			System.err.println(e.getMessage());
			setException(e);
			e.printStackTrace();

		}
		return null;
	}

	public static String getPublicIp() {
		return getString("https://api.ipify.org");
	}

	public static returnObject invoike(String callMod) {
		CallMod call = new CallMod();
		Map map = new HashMap();
		return call.invoke(callMod, map);
	}

	public static synchronized returnObject invoike(String callMod, Map callData) {
		CallMod call = new CallMod();
		Map<Integer, Object> map = new TreeMap<Integer, Object>();
		map.putAll(callData);
		return call.invoke(callMod, map);
	}
}
