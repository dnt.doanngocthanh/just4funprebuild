package com.dano.j4F.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TIMESTAMP {
	private Date date;
	private Calendar calendar;
	private SimpleDateFormat fmt_timestamp;

	public TIMESTAMP() {
		fmt_timestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		calendar = Calendar.getInstance();
	}

	public String getCurrentTime() {
		return fmt_timestamp.format(new Date());
	}

	public String getTimestamp(Date date) {
		return fmt_timestamp.format(date);
	}

	public String getTimestamp(String date) {
		return fmt_timestamp.format(date);
	}

	public String addDate(String date, int day) {
		calendar.setTime(getDate(date));
		calendar.add(Calendar.DAY_OF_MONTH, day);
		Date newDate = calendar.getTime();
		return fmt_timestamp.format(newDate);
	}

	public Date getDate(String timestamp) {
		try {
			date = fmt_timestamp.parse(timestamp);
		} catch (ParseException e) {
			System.err.println(e.getMessage());
			return null;
		}
		return date;
	}

	public boolean checkTimeEvent(String timeBegin, String timeStop) {
		Date currentTime = getDate(getCurrentTime());
		Date startTime = getDate(timeBegin);
		Date endTime = getDate(timeStop);
		return currentTime.after(startTime) && currentTime.before(endTime);
	}

	public static void main(String[] args) {
		TIMESTAMP time = new TIMESTAMP();
		System.out.println(time.getCurrentTime());
	}
}
