package com.dano.j4F.account;

import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;

import org.apache.commons.lang3.ObjectUtils;

import com.dano.j4F.batch.batchDate;
import com.dano.j4F.jdbc.DataSet;
import com.dano.j4F.plugin.API_PASSWORD_SECURITY;
import com.dano.j4F.plugin.returnObject;
import com.dano.j4F.sevice.DB_Module;

public class userObject extends DB_Module {
	private Cookie cookie;
	public returnObject msg;
	private batchDate DATE = new batchDate();

	public Cookie getCookie() {
		return cookie;
	}

	public void setCookie(Cookie cookie) {
		this.cookie = cookie;
	}

	public returnObject resignAccount(userObject_vo vo) {
		DataSet ds = getDataSet();
		msg = new returnObject();
		try {
			ds.setField("USERNAME", vo.getUsername());
			List<Map<String, Object>> li = ds.searchAndRetrieve("SELECT * FROM ACCOUNT WHERE USERNAME =:USERNAME");
			if (li.size() > 0) {
				msg.setMsgDescs("Người dùng đã được đăng ký trước đó! xin vui lòng nhập username khác.");
				msg.setReturnCode(-1);
				return msg;
				// throw new Exception("Người dùng đã được đăng ký trước đó! xin vui lòng nhập
				// username khác.");
			}
			ds.setField("PASSWORD", vo.getPassword());
			ds.setField("FULLNAME", vo.getEmp_name());
			ds.setField("EMAIL", vo.getEmail());
			ds.setField("MOBILE", vo.getMobile());
			ds.setField("EMP_NO", vo.getEmp_code());
			ds.setField("IS_BAN", vo.getIs_ban());
			ds.setField("IS_DEL", vo.getIs_del());
			ds.update("INSERT INTO ACCOUNT (USERNAME, PASSWORD, FULLNAME, IS_DEL, IS_BAN, EMAIL, MOBILE, EMP_NO) VALUES"
					+ " (:USERNAME, :PASSWORD, :FULLNAME, :IS_DEL, :IS_BAN, :EMAIL, :MOBILE, :EMP_NO)");
			msg.setReturnData(vo);
			return msg;

		} catch (Exception e) {
			msg.setException(e);
			return msg;
		}

	}

	@SuppressWarnings("deprecation")
	public userObject_vo findUser(String username) {
		DataSet ds = getDataSet();
		ds.setField("USERNAME", username);
		List<Map<String, Object>> li = ds.searchAndRetrieve(
				"SELECT A.*,B.TOKEN  FROM ACCOUNT A LEFT JOIN TOKEN_ACCOUNT B ON A.USERNAME =B.ACOUNT_ID  WHERE A.USERNAME =:USERNAME ;");
		userObject_vo userObj = new userObject_vo();
		for (Map<String, Object> map : li) {
			userObj.setUsername(ObjectUtils.toString(map.get("username")));
			userObj.setPassword_DB(ObjectUtils.toString(map.get("password")));
			userObj.setEmp_code(ObjectUtils.toString(map.get("emp_no")));
			userObj.setEmp_name(ObjectUtils.toString(map.get("fullname")));
			userObj.setEmail(ObjectUtils.toString(map.get("email")));
			userObj.setIs_ban(ObjectUtils.toString(map.get("is_ban")));
			userObj.setIs_del(ObjectUtils.toString(map.get("is_del")));
			userObj.setMobile(ObjectUtils.toString(map.get("mobile")));
			userObj.setToken(ObjectUtils.toString(map.get("token")));
		}
		return userObj;
	}

	@SuppressWarnings("deprecation")
	public boolean checkPassword(userObject_vo loginVo) throws Exception {
		userObject_vo userObj = new userObject_vo();
		userObj = findUser(loginVo.getUsername());
		userObj.setBgn_login_time(DATE.getCurrentTimestamp());
		if (new API_PASSWORD_SECURITY().checkPassword(loginVo.getPassword(), userObj.getPassword())) {
			return true;
		}
		return false;
	}

	public boolean checkToken(String TOKEN) {
		String SQL1 = "SELECT TOKEN FROM TOKEN_ACCOUNT WHERE TOKEN =:TOKEN AND BGN_TIME <= NOW() AND END_TIME >= NOW();";
		DataSet ds = getDataSet();
		ds.setField("TOKEN", TOKEN);
		List<Map<String, Object>> li = ds.searchAndRetrieve(SQL1);
		if (li.size() > 0) {
			return true;
		}
		return false;
	}

	public returnObject createTokenAndLog(userObject_vo loginVo) {
		msg = new returnObject();
		DataSet ds = getDataSet();
		String SQL1 = "SELECT * FROM TOKEN_ACCOUNT WHERE ACOUNT_ID =:ACOUNT_ID";
		try {
			ds.setField("ACOUNT_ID", loginVo.getUsername());
			List<Map<String, Object>> li = ds.searchAndRetrieve(SQL1);
			if (li != null) {
				if (li.size() > 0) {
					try {
						String SQLDELETE = "DELETE FROM TOKEN_ACCOUNT where ACOUNT_ID=:ACOUNT_ID";
						ds.setField("ACOUNT_ID", loginVo.getUsername());
						ds.update(SQLDELETE);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
				String SQL2 = "INSERT INTO TOKEN_ACCOUNT (ACOUNT_ID, TOKEN, IP_REAL, IP_FORWARDED, BGN_TIME, END_TIME, LOGIN_TYPE) "
						+ "VALUES (:ACOUNT_ID, :TOKEN, :IP_REAL, :IP_FORWARDED, :BGN_TIME, :END_TIME, :LOGIN_TYPE);";
				try {
					ds.setField("ACOUNT_ID", loginVo.getUsername());
					ds.setField("TOKEN", new API_PASSWORD_SECURITY().encodePassword(loginVo.getUsername()));
					ds.setField("IP_REAL", loginVo.getX_real_ip());
					ds.setField("IP_FORWARDED", loginVo.getX_forwarded_for());
					ds.setField("BGN_TIME", loginVo.getBgn_login_time());
					ds.setField("END_TIME", loginVo.getEnd_login_time());
					ds.setField("LOGIN_TYPE", loginVo.getFunction_login());
					ds.update(SQL2);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return msg;
	}

	public static void main(String[] args) {
		userObject_vo vo = new userObject_vo();
		userObject userMod = new userObject();
		vo.setUsername("123433");
		vo.setPassword_DB("3");
		vo.setFunction_login("APP");
		try {
			System.out.println(userMod.checkPassword(vo));
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
