package com.dano.j4F.account;

import com.dano.j4F.batch.batchDate;
import com.dano.j4F.plugin.API_PASSWORD_SECURITY;
import com.dano.j4F.plugin.VoTooL;

public class userObject_vo {
	private String username;
	private String password;
	private String token;
	private String emp_code;
	private String emp_name;
	private String email;
	private String mobile;
	private String role_id;
	private String is_del;
	private String is_ban;
	private String bgn_login_time;
	private String end_login_time;
	private String last_update_time;
	private String x_real_ip;
	private String x_forwarded_for;
	private String function_login;
	public int timeSession = 30;
	private batchDate DATE = new batchDate();

	public String getX_real_ip() {
		return x_real_ip;
	}

	public void setX_real_ip(String x_real_ip) {
		this.x_real_ip = x_real_ip;
	}

	public String getX_forwarded_for() {
		return x_forwarded_for;
	}

	public void setX_forwarded_for(String x_forwarded_for) {
		this.x_forwarded_for = x_forwarded_for;
	}

	public String getFunction_login() {
		return function_login;
	}

	public void setFunction_login(String function_login) {
		this.function_login = function_login;
	}

	public String getLast_update_time() {
		this.last_update_time = new batchDate().getCurrentTimestamp();
		return last_update_time;
	}

	public void setLast_update_time() {
		this.last_update_time = new batchDate().getCurrentTimestamp();
	}

	public int getTimeSession() {
		return timeSession;
	}

	public void setTimeSession(int timeSession) {
		this.timeSession = timeSession;
	}

	public String getBgn_login_time() {
		return bgn_login_time;
	}

	public void setBgn_login_time(String bgn_login_time) {
		this.bgn_login_time = bgn_login_time;
		this.end_login_time = DATE.addMinutesTimeStamp(bgn_login_time, this.timeSession);
	}

	public String getEnd_login_time() {
		return end_login_time;
	}

	public void setPassword_DB(String password) {
		this.password = password;
	}

	public String getIs_del() {
		return is_del;
	}

	public void setIs_del(String is_del) {
		this.is_del = is_del;
	}

	public String getIs_ban() {
		return is_ban;
	}

	public void setIs_ban(String is_ban) {
		this.is_ban = is_ban;
	}

	public String getUsername() throws Exception {
		if (username == null) {
			throw new Exception("username không được bỏ trống!");
		}
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = API_PASSWORD_SECURITY.encodePassword(password);
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getEmp_code() {
		return emp_code;
	}

	public void setEmp_code(String emp_code) {
		this.emp_code = emp_code;
	}

	public String getEmp_name() {
		return emp_name;
	}

	public void setEmp_name(String emp_name) {
		this.emp_name = emp_name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getRole_id() {
		return role_id;
	}

	public void setRole_id(String role_id) {
		this.role_id = role_id;
	}

	public static void main(String[] args) {
		userObject_vo vo = new userObject_vo();
		vo.setUsername("123423");
		vo.setPassword("3");
		vo.setEmp_name("đoàn ngọc thành");
		vo.setIs_ban("1");
		vo.setIs_del("1");
		vo.setEmail("123124");
		vo.setMobile("0123456");
		vo.setEmp_code("0100110");

		userObject mod = new userObject();
		System.out.println(new VoTooL().getJSONString(mod.resignAccount(vo)));

	}

}
