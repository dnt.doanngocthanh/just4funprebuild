package com.dano.j4F.chatbot;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;

import com.dano.j4F.jdbc.DataSet;
import com.dano.j4F.sevice.DB_Module;

public class BotMenu extends DB_Module {
	private static List<Map<String, Object>> listMenuItems;
	private static ReplyKeyboardMarkup replyKeyboardMarkup;
	private static List<KeyboardRow> keyboard;
	static {
		replyKeyboardMarkup = new ReplyKeyboardMarkup();
		replyKeyboardMarkup.setSelective(true);
		replyKeyboardMarkup.setResizeKeyboard(true);
		replyKeyboardMarkup.setOneTimeKeyboard(false);
	}

	public BotMenu() {
		listMenuItems = new ArrayList<Map<String, Object>>();
		keyboard = new ArrayList<KeyboardRow>();
	}

	private static void snapShotMenu(String botUsername) {
		DataSet ds = getDataSet();
		listMenuItems = ds.searchAndRetrieve("SELECT * FROM TELEGRAM_MENU_FUCTION");
		// ds.setField("BOT_ID", botUsername);
	}

	private static void addMenu(Map map) {

	}

	public KeyboardRow addCommand(String command) {
		KeyboardRow row = new KeyboardRow();
		row.add(new KeyboardButton(command));
		keyboard.add(row);
		return row;
	}

	public SendMessage getBotMenu() {
		SendMessage message = new SendMessage();
		replyKeyboardMarkup.setKeyboard(keyboard);
		message.setReplyMarkup(replyKeyboardMarkup);
		return message;
	}

	private SendMessage displayMainMenu(long chatId) {
		ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
		replyKeyboardMarkup.setSelective(true);
		replyKeyboardMarkup.setResizeKeyboard(true);
		replyKeyboardMarkup.setOneTimeKeyboard(false);
		List<KeyboardRow> keyboard = new ArrayList();

		KeyboardRow row1 = new KeyboardRow();
		row1.add(new KeyboardButton("/help"));
		KeyboardRow row2 = new KeyboardRow();
		row2.add(new KeyboardButton("/start"));
		keyboard.add(row1);
		keyboard.add(row2);
		replyKeyboardMarkup.setKeyboard(keyboard);
		SendMessage message = new SendMessage();
		message.setChatId(chatId);
		message.setText("Đây là menu lệnh.");
		message.setReplyMarkup(replyKeyboardMarkup);
		return message;
	}

}
