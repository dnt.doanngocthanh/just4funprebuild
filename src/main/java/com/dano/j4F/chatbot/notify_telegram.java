package com.dano.j4F.chatbot;

import java.io.File;
import java.util.List;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.commands.SetMyCommands;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.methods.send.SendVideo;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.commands.BotCommand;
import org.telegram.telegrambots.meta.api.objects.commands.scope.BotCommandScopeDefault;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import com.dano.j4F.config.Enviroment;
import com.dano.j4F.plugin.VoTooL;
import com.dano.j4F.plugin.fileExtensions;

public class notify_telegram extends TelegramLongPollingBot {

	private String BOT_TOKEN;
	private String BOT_USERNAME;
	private String CHAT_ID;
	private org.telegram.telegrambots.meta.api.objects.Message Message;

	public org.telegram.telegrambots.meta.api.objects.Message getMessage() {
		return Message;
	}

	public void setMessage(org.telegram.telegrambots.meta.api.objects.Message message) {
		Message = message;
	}

	@Override
	public String getBotUsername() {
		return BOT_USERNAME;
	}

	@Override
	public String getBotToken() {
		return BOT_TOKEN;
	}

	public void setBOT_TOKEN(String bOT_TOKEN) {
		BOT_TOKEN = bOT_TOKEN;
	}

	public void setBOT_USERNAME(String bOT_USERNAME) {
		BOT_USERNAME = bOT_USERNAME;
	}

	public String getCHAT_ID() {
		return CHAT_ID;
	}

	public void setCHAT_ID(String cHAT_ID) {
		CHAT_ID = cHAT_ID;
	}

	@Override
	public void onUpdateReceived(Update update) {

	}

	public boolean send(SendMessage message) {
		try {
			message.setText("default");
			setMessage(execute(message));
			logBotSender();
			System.out.println("Tin nhắn đã được gửi thành công!");
			return true;
		} catch (TelegramApiException e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean sendMessage(String message) {
		SendMessage sendMessage = new SendMessage();
		sendMessage.setChatId(CHAT_ID);
		sendMessage.setText(message);

		try {
			setMessage(execute(sendMessage));
			logBotSender();
			System.out.println("Tin nhắn đã được gửi thành công!");
			return true;
		} catch (TelegramApiException e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean sendPhoto(File photoFile, String caption) {
		SendPhoto sendPhoto = new SendPhoto();
		sendPhoto.setChatId(CHAT_ID);
		sendPhoto.setPhoto(new InputFile(photoFile));
		sendPhoto.setCaption(caption);

		try {
			setMessage(execute(sendPhoto));
			logBotSender();
			System.out.println("Hình ảnh đã được gửi thành công!");
			return true;
		} catch (TelegramApiException e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean sendVideo(File videoFile, String caption) {
		SendVideo sendVideo = new SendVideo();
		sendVideo.setChatId(CHAT_ID);
		sendVideo.setVideo(new InputFile(videoFile));
		sendVideo.setCaption(caption);
		try {
			setMessage(execute(sendVideo));
			logBotSender();
			System.out.println("Video đã được gửi thành công!");
			return true;
		} catch (TelegramApiException e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean sendMessageHtml() {
		SendMessage sendMessage = new SendMessage();
		sendMessage.setChatId(CHAT_ID);

		// Định dạng tin nhắn với các định dạng khác nhau
		sendMessage.enableHtml(true); // Cho phép sử dụng HTML trong tin nhắn

		// Xuống dòng
		sendMessage.setText("Dòng 1\nDòng 2");

		// Bôi đậm
		// sendMessage.setText("<b>Bôi đậm</b>");

		// Đổi màu
		// sendMessage.setText("<font color=\"red\">Đổi màu đỏ</font>");

		try {
			Message = execute(sendMessage);
			setMessage(Message);
			logBotSender();
			System.out.println("Tin nhắn đã được gửi thành công!");
			return true;
		} catch (TelegramApiException e) {
			e.printStackTrace();
		}
		return false;
	}

	public void logBotSender() {
		if (getMessage() != null) {
			System.out.println(new VoTooL().getJSONString(getMessage()));
		}
	}

	public boolean sendDocument(File documentFile) {
		SendDocument sendDocument = new SendDocument();
		sendDocument.setChatId(CHAT_ID);
		sendDocument.setDocument(new InputFile(documentFile));
		try {
			setMessage(execute(sendDocument));
			logBotSender();
			System.out.println("Tài liệu đã được gửi thành công!");
			return true;
		} catch (TelegramApiException e) {
			e.printStackTrace();
		}
		return false;
	}

	public void sendFile(File file, String desc) {
		String fileExtension = fileExtensions.getFileExtension(file);
		if (fileExtensions.isTextFile(fileExtension)) {
			// Gửi văn bản
			sendDocument(file);
		} else if (fileExtensions.isDocumentFile(fileExtension)) {
			// Gửi tài liệu
			sendDocument(file);
		} else if (fileExtensions.isImageFile(fileExtension)) {
			// Gửi hình ảnh
			sendPhoto(file, desc);
		} else if (fileExtensions.isVideoFile(fileExtension)) {
			// Gửi video
			sendVideo(file, desc);
		} else {
			System.out.println("Không hỗ trợ gửi loại tệp tin này.");
		}
	}

	public void changeBotName(String newBotName) {
		BotCommand startCommand = new BotCommand("start", newBotName);
		SetMyCommands setMyCommands = new SetMyCommands();
		setMyCommands.setCommands(List.of(startCommand));
		setMyCommands.setScope(new BotCommandScopeDefault());

		try {
			execute(setMyCommands);
		} catch (TelegramApiException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		BotMenu bot = new BotMenu();
		bot.addCommand("/test");
		bot.addCommand("/thử");
		bot.addCommand("/bot");
		SendMessage message = bot.getBotMenu();
		message.setChatId(Enviroment.notyfiDefault.getCHAT_ID());
		Enviroment.notyfiDefault.send(message);

	}

}