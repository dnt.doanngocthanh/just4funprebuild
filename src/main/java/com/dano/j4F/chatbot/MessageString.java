package com.dano.j4F.chatbot;

public class MessageString {
	private static String nextLine = "\n";
	private String messenger;

	public MessageString() {
		this.messenger = "";
	}

	public String addTextNextLine(String text) {
		if (messenger.equals("")) {
			this.messenger = text;
		} else {
			this.messenger = messenger + nextLine + text;
		}
		return messenger;
	}

	public String boldText(String text) {
		return "<b>" + text + "</b>";
	}

	public String getMessenger() {
		return messenger;
	}

	public static void main(String[] args) {
		MessageString ms = new MessageString();
		ms.addTextNextLine("1");
		ms.addTextNextLine(ms.boldText("2"));
		ms.addTextNextLine("3");
		System.out.println(ms.getMessenger().toString());
	}
}
