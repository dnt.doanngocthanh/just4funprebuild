package com.dano.j4F.chatbot;

import java.util.HashMap;
import java.util.Map;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

import com.dano.j4F.config.Enviroment;
import com.dano.j4F.sevice.ConfigManager;

public class BotBatch extends TelegramLongPollingBot {
	private String BOT_USERNAME;
	private String BOT_TOKEN;
	public static TelegramBotsApi telegramBotsApi;
	private static Map<String, String> mapMenuBatchController;
	static {
		mapMenuBatchController = new HashMap<String, String>();
		mapMenuBatchController.put("listBatch", "/listBatch");
		mapMenuBatchController.put("stopBatch", "/stopBatch/{0}");
		mapMenuBatchController.put("pauseBatch", "/pauseBatch/{0}");
		mapMenuBatchController.put("unPauseBatch", "/unPauseBatch/{0}");
		mapMenuBatchController.put("rerunBatch", "/rerunBatch/{0}");
		try {
			telegramBotsApi = new TelegramBotsApi(DefaultBotSession.class);
		} catch (TelegramApiException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public BotBatch(String username, String token) throws TelegramApiException {
		this.BOT_USERNAME = username;
		this.BOT_TOKEN = token;
		telegramBotsApi.registerBot(this);
	}

	public static String getStringBatchNoneActive(String batchName, String status) {
		MessageString mess = new MessageString();
		mess.addTextNextLine("BatchName: " + batchName);
		mess.addTextNextLine("StatusBatch: " + status);
		mess.addTextNextLine("Action: /startBatch" + batchName);
		return mess.getMessenger();
	}

	@Override
	public void onUpdateReceived(Update update) {
		// Xử lý thông tin cập nhật
		if (update.hasMessage() && update.getMessage().hasText()) {
			// Lấy thông tin tin nhắn từ người dùng
			String messageText = update.getMessage().getText();
			long chatId = update.getMessage().getChatId();

			// Phản hồi lại tin nhắn từ người dùng
			SendMessage message = new SendMessage();

			message.setChatId(String.valueOf(chatId));
			message.setText("Bạn đã gửi tin nhắn: " + messageText);

			try {
				// Gửi tin nhắn phản hồi
				execute(message);
			} catch (TelegramApiException e) {
				e.printStackTrace();
			}
		}
	}

	public void SendMessage(String chatId, String Mess) {
		SendMessage message = new SendMessage();
		message.setChatId(String.valueOf(chatId));
		message.setText(Mess);
		message.setParseMode("HTML");
		try {
			// Gửi tin nhắn phản hồi
			execute(message);
		} catch (TelegramApiException e) {
			e.printStackTrace();
		}
	}

	@Override
	public String getBotUsername() {
		return BOT_USERNAME;
	}

	@Override
	public String getBotToken() {
		return BOT_TOKEN;
	}

	public static String createJobMessage(String jobName, boolean isActive, String onLink, String offLink) {
		StringBuilder messageBuilder = new StringBuilder();
		messageBuilder.append(String.format("%-12s: %s\n", "JOB_NAME", jobName));

		// Trạng thái hoạt động
		String status = isActive ? "Đang hoạt động" : "Không hoạt động";
		messageBuilder.append(String.format("%-12s: %s\n", "RUNNING", status));

		// Link bật công việc
		messageBuilder.append(String.format("%-12s: %s\n", "ACTIVE", onLink));

		// Link tắt công việc
		messageBuilder.append(String.format("%-12s: %s\n", "DISABLE", offLink));
		return messageBuilder.toString();
	}

	public static void main(String[] args) {
		String username = ConfigManager.getProperty("com.dano.j4F.chatbot.notify_telegram.stag1.username");
		String token = ConfigManager.getProperty("com.dano.j4F.chatbot.notify_telegram.stag1.token");
		BotBatch bot;
		String jobName = "Công việc hàng ngày";
		boolean isActive = true;
		String onLink = "https://example.com/on";
		String offLink = "https://example.com/off";
		try {
			bot = new BotBatch(username, token);
			String htmlTable = "<table>" + "<tr><th>Column 1</th><th>Column 2</th></tr>"
					+ "<tr><td>Row 1, Column 1</td><td>Row 1, Column 2</td></tr>"
					+ "<tr><td>Row 2, Column 1</td><td>Row 2, Column 2</td></tr>"
					+ "<tr><td>Row 2, Column 1</td><td>Row 2, Column 2</td></tr>" + "</table>";

			bot.SendMessage(Enviroment.notyfiDefault.getCHAT_ID(),
					"<a href='/stopJob/AAAAA'>Đây là một liên kết</a>");
		} catch (TelegramApiException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Bắt đầu lắng nghe thông tin cập nhật từ Telegram

	}
}