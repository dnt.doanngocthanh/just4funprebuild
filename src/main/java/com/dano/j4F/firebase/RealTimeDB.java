package com.dano.j4F.firebase;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import org.apache.log4j.BasicConfigurator;

import com.dano.j4F.config.Enviroment;
import com.dano.j4F.plugin.VoTooL;
import com.dano.j4F.sevice.DB_Module;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RealTimeDB extends DB_Module {
	private static FirebaseDatabase firebaseDatabase;
	private DatabaseReference databaseReference;
	private DatabaseReference childReference;
	private CountDownLatch countDownLatch;
	private static InputStream API_KEY_InputStream;
	private static FirebaseOptions firebaseOptions;

	public void updateSetting(String API_KEY, String URL_CONFIG) throws IOException {
		API_KEY_InputStream = new ByteArrayInputStream(API_KEY.getBytes());
		firebaseDatabase = null;
		if (firebaseDatabase == null) {
			try {
				firebaseOptions = new FirebaseOptions.Builder()
						.setCredentials(GoogleCredentials.fromStream(API_KEY_InputStream)).setDatabaseUrl(URL_CONFIG)
						.build();
				firebaseDatabase = initFirebase();

			} catch (IOException e) {
				e.printStackTrace();
				throw e;
			}
		}
	}

	public RealTimeDB() {

	}

	private static FirebaseDatabase initFirebase() {
		if (FirebaseApp.getApps().isEmpty()) {
			FirebaseApp.initializeApp(firebaseOptions);
		}
		firebaseDatabase = FirebaseDatabase.getInstance();
		return firebaseDatabase;
	}

	public synchronized void saveBatchStatusRealTime(String key, Map map) throws InterruptedException {
		try {
			map.put("USER_UPDATE", this.getClass().getSimpleName());
			if (firebaseDatabase == null) {
				firebaseDatabase = initFirebase();
			} else {
				databaseReference = firebaseDatabase.getReference("batchs/");
				childReference = databaseReference.child(key);
				countDownLatch = new CountDownLatch(1);
				childReference.setValue(map, new DatabaseReference.CompletionListener() {
					@Override
					public void onComplete(DatabaseError de, DatabaseReference dr) {
						countDownLatch.countDown();

					}
				});

				countDownLatch.await();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			countDownLatch = null;
		}
	}

	List<Map<String, Object>> resultList = new ArrayList<>();

	public RealTimeDB getFireBaseConfig(String key) {
		RealTimeDB RealTimeDB = new RealTimeDB();
		try {
			RealTimeDB.updateSetting(get999config("FIREBASE_APIKEY_" + key.toUpperCase()),
					get999config("FIREBASE_URL_" + key.toUpperCase()));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return RealTimeDB;
	}

	public static void main(String[] args) {
		BasicConfigurator.configure();

	}

	public void getBatchStatusRealTime() {
		try {
			if (firebaseDatabase == null) {
				firebaseDatabase = initFirebase();
			} else {
				databaseReference = firebaseDatabase.getReference("batchs/");
				countDownLatch = new CountDownLatch(1);

				databaseReference.addChildEventListener(new ChildEventListener() {
					@Override
					public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {

						Map voMap = new HashMap();
						try {
							voMap = new VoTooL().objToMap(dataSnapshot.getValue());
							resultList.add(voMap);
							System.out.println("nè ha: " + voMap);
						} catch (Exception e) {

						}

					}

					@Override
					public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {
						Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();
						resultList.add(map);
					}

					@Override
					public void onChildRemoved(DataSnapshot dataSnapshot) {
						// Do nothing
					}

					@Override
					public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
						// Do nothing
					}

					@Override
					public void onCancelled(DatabaseError databaseError) {
						countDownLatch.countDown();
					}
				});

				countDownLatch.await();

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			countDownLatch.countDown();
		}

	}

}