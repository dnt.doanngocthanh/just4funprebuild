package com.dano.j4F.plugin;

import java.io.File;

public class fileExtensions {
	public static String getFileExtension(File file) {
		String fileName = file.getName();
		int lastDotIndex = fileName.lastIndexOf(".");
		if (lastDotIndex != -1 && lastDotIndex < fileName.length() - 1) {
			return fileName.substring(lastDotIndex + 1).toLowerCase();
		}
		return "";
	}

	public static boolean isTextFile(String fileExtension) {
		String[] textExtensions = { "txt", "doc", "docx", "pdf" };
		for (String extension : textExtensions) {
			if (extension.equals(fileExtension)) {
				return true;
			}
		}
		return false;
	}

	public static boolean isDocumentFile(String fileExtension) {
		String[] documentExtensions = { "doc", "docx", "pdf" };
		for (String extension : documentExtensions) {
			if (extension.equals(fileExtension)) {
				return true;
			}
		}
		return false;
	}

	public static boolean isAudioFile(String fileExtension) {
		String[] audioExtensions = { "mp3", "wav" };
		for (String extension : audioExtensions) {
			if (extension.equals(fileExtension)) {
				return true;
			}
		}
		return false;
	}

	public static boolean isImageFile(String fileExtension) {
		String[] imageExtensions = { "jpg", "jpeg", "png", "gif" };
		for (String extension : imageExtensions) {
			if (extension.equals(fileExtension)) {
				return true;
			}
		}
		return false;
	}

	public static boolean isVideoFile(String fileExtension) {
		String[] videoExtensions = { "mp4", "avi", "mov" };
		for (String extension : videoExtensions) {
			if (extension.equals(fileExtension)) {
				return true;
			}
		}
		return false;
	}
}
