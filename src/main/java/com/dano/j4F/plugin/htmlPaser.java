package com.dano.j4F.plugin;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class htmlPaser {
	public static String parseHtmlTable(String htmlTable) {
	    StringBuilder tableBuilder = new StringBuilder();

	    Document doc = Jsoup.parse(htmlTable);
	    Elements rows = doc.select("table tr");

	    int maxColumnWidth = 20; // Độ rộng tối đa cho mỗi ô
	    int maxRowLength = 0;

	    // Tính toán độ rộng tối đa của mỗi cột
	    int[] columnWidths = new int[maxColumnWidth];
	    for (Element row : rows) {
	        Elements cells = row.select("td,th");

	        // Đảm bảo không vượt quá độ rộng tối đa cho mỗi ô
	        for (int i = 0; i < cells.size() && i < maxColumnWidth; i++) {
	            Element cell = cells.get(i);
	            columnWidths[i] = Math.max(columnWidths[i], cell.text().length());
	        }

	        maxRowLength = Math.max(maxRowLength, cells.size());
	    }

	    // Giảm độ rộng của các ô để đảm bảo độ rộng đồng đều
	    for (int i = 0; i < columnWidths.length; i++) {
	        columnWidths[i] = Math.min(columnWidths[i], maxColumnWidth);
	    }

	    // Tạo đường kẻ ngang
	    String horizontalLine = "+" + "-".repeat(maxColumnWidth + 2) + "+";

	    // Xây dựng bảng với độ rộng đồng đều
	    tableBuilder.append(horizontalLine).append("\n");
	    for (Element row : rows) {
	        Elements cells = row.select("td,th");
	        int cellCount = cells.size();

	        tableBuilder.append("| ");
	        for (int i = 0; i < cellCount && i < columnWidths.length; i++) {
	            Element cell = cells.get(i);
	            String cellText = cell.text();

	            // Đảm bảo độ rộng của ô
	            if (cellText.length() > columnWidths[i]) {
	                cellText = cellText.substring(0, columnWidths[i] - 3) + "...";
	            } else {
	                cellText = String.format("%-" + columnWidths[i] + "s", cellText);
	            }

	            tableBuilder.append(cellText).append(" | ");
	        }

	        // Bổ sung khoảng trống cho các cột bị thiếu
	        for (int i = cellCount; i < maxRowLength; i++) {
	            tableBuilder.append(" ".repeat(maxColumnWidth)).append(" | ");
	        }

	        tableBuilder.append("\n");

	        // Tạo đường kẻ ngang giữa các hàng
	        if (tableBuilder.indexOf(horizontalLine) == -1) {
	            tableBuilder.append(horizontalLine).append("\n");
	        }
	    }

	    tableBuilder.append(horizontalLine);

	    return tableBuilder.toString();
	}
	public static void main(String[] args) {
		String htmlTable = "<table>" + "<tr><th>Column 1</th><th>Column 2</th><th>Column 3</th></tr>"
				+ "<tr><td>Row 1, Column 1</td><td>Row 1, Column 2</td><td>Row 1, Column 2</td></tr>"
				+ "<tr><td>Row 2, Column 1</td><td>Row 2, Column 2</td><td>Row 1, Column 2</td></tr>"
				+ "<tr><td>Row 2, Column 1</td><td>Row 2, Column 2</td><td>Row 1, Column 2</td></tr>" + "</table>";

		String parsedTable = parseHtmlTable(htmlTable);
		System.out.println(parsedTable);
	}
}
