package com.dano.j4F.plugin;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class API_PASSWORD_SECURITY {
	private static final BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
	public static synchronized String encodePassword(String password) {
		return passwordEncoder.encode(password);
	}
	public static synchronized boolean checkPassword(String rawPassword, String encodedPassword) {
		return passwordEncoder.matches(rawPassword, encodedPassword);
	}
}
