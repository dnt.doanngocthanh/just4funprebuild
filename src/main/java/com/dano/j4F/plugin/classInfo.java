package com.dano.j4F.plugin;

public class classInfo {
	private static String className;
	private static String packageName;
	private static String packagePath;
	private static String projectName;
	public classInfo(Class<?> clazz) {
		setClassName(clazz.getSimpleName());
		setPackageName(clazz.getPackage().getName());
		setPackagePath(packageName.replace("." + className, ""));
		String[] packageParts = getPackagePath().split("\\.");
		String projectName = packageParts[packageParts.length - 1];
		setProjectName(projectName);
	}
	public static String getClassName() {
		return className;
	}
	public static void setClassName(String className) {
		classInfo.className = className;
	}

	public static String getPackageName() {
		return packageName;
	}

	public static void setPackageName(String packageName) {
		classInfo.packageName = packageName;
	}

	public static String getPackagePath() {
		return packagePath;
	}

	public static void setPackagePath(String packagePath) {
		classInfo.packagePath = packagePath;
	}

	public static String getProjectName() {
		return projectName;
	}

	public static void setProjectName(String projectName) {
		classInfo.projectName = projectName;
	}

	public static String getProjectName(Class<?> clazz) {
		className = clazz.getSimpleName();
		packageName = clazz.getPackage().getName();
		packagePath = packageName.replace("." + className, "");
		String[] packageParts = packagePath.split("\\.");
		String projectName = packageParts[packageParts.length - 1];

		return projectName;
	}
}
