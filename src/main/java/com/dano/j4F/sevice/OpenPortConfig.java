package com.dano.j4F.sevice;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.dano.j4F.jdbc.DataSet;

public class OpenPortConfig extends DB_Module {
	private static List<Map<String, Object>> listPortNeedOpen;
	private static final String alert1 = "Port: {0} \n Sever: {1}";
	private static final String sqlselectconfig = "SELECT * FROM SYS_PORT_CONFIG";
	public static final Map<Object, Object> mapDataSet = new HashMap<Object, Object>();
	static {
		listPortNeedOpen = getListPortFromDB();
		mapDataSet.put(PARAM.PORT, "0");
		mapDataSet.put(PARAM.SEVER, "1");

	}

	public static List<Map<String, Object>> getListPortFromDB() {
		DataSet ds = getDataSet();
		return ds.searchAndRetrieve(sqlselectconfig);
	}

	public boolean open(String port) {
		try {
			String command = "ufw allow " + port;
			Process process = Runtime.getRuntime().exec(command);
			int exitCode = process.waitFor();
			if (exitCode == 0) {
				System.out.println("Đã mở cổng trên tường lửa thành công");
				return true;
			}
		} catch (IOException | InterruptedException e) {
			return false;
		}
		return false;
	}
	public boolean openWithAccount(String port) {
	    try {
	        String[] command = { "sudo", "-S", "ufw", "allow", port };
	        ProcessBuilder processBuilder = new ProcessBuilder(command);
	        
	        // Thực hiện chuyển đổi sang tài khoản có quyền sudo
	        processBuilder.environment().put("SUDO_USER", "xthanhxvn");
	        
	        // Gửi mật khẩu sudo nếu yêu cầu
	        processBuilder.redirectErrorStream(true);
	        Process process = processBuilder.start();
	        OutputStream outputStream = process.getOutputStream();
	        outputStream.write("your_sudo_password\n".getBytes());
	        outputStream.flush();

	        int exitCode = process.waitFor();
	        if (exitCode == 0) {
	            System.out.println("Đã mở cổng trên tường lửa thành công");
	            return true;
	        }
	    } catch (IOException | InterruptedException e) {
	        return false;
	    }
	    return false;
	}

	public static void main(String[] args) {
		System.out.println(mapDataSet.get(PARAM.PORT));
	}

	private enum statusLevel {
		SUCCESS, ERROR
	}

	private enum PARAM {
		PORT, SEVER, EROR, SUCCESS
	}
}
