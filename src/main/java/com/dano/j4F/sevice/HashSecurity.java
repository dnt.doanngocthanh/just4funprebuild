package com.dano.j4F.sevice;

import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import com.dano.j4F.jdbc.DataSet;

public class HashSecurity extends DB_Module {
	

	private static final String SIGNATURE_ALGORITHM = "SHA256withRSA";
	private PrivateKey privateKey;
	private PublicKey publicKey;

	public static KeyPair generateKeyPair() throws Exception {
		KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
		keyPairGenerator.initialize(2048); // Độ dài khóa: 2048 bits
		return keyPairGenerator.generateKeyPair();
	}

	public static byte[] signData(byte[] data, PrivateKey privateKey) throws Exception {
		Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);
		signature.initSign(privateKey);
		signature.update(data);
		return signature.sign();
	}

	public static boolean verifySignature(byte[] data, byte[] signature, PublicKey publicKey) throws Exception {
		Signature verifier = Signature.getInstance(SIGNATURE_ALGORITHM);
		verifier.initVerify(publicKey);
		verifier.update(data);
		return verifier.verify(signature);
	}

	public static PublicKey stringToPublicKey(String publicKeyStr) throws Exception {
		byte[] publicKeyBytes = Base64.getDecoder().decode(publicKeyStr);
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(publicKeyBytes);
		return keyFactory.generatePublic(publicKeySpec);
	}

	public static PrivateKey stringToPrivateKey(String privateKeyStr) throws Exception {
		byte[] privateKeyBytes = Base64.getDecoder().decode(privateKeyStr);
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(privateKeyBytes);
		return keyFactory.generatePrivate(privateKeySpec);
	}

	private static String keyPairToString(Key key) {
		return Base64.getEncoder().encodeToString(key.getEncoded());
	}

	public static String resignNewKey(String regsin) {
		String uniStr = ramdom.UUID().replaceAll("-", "");
		KeyPair keyPair;
		DataSet ds = getDataSet();
		String sql = "INSERT INTO HASH_SECURITY (ID, VALUE, PRIVATE_KEY, PUBLIC_KEY) VALUES(:ID, :VALUE, :PRIVATE_KEY, :PUBLIC_KEY);";
		try {
			keyPair = generateKeyPair();
			ds.setField("ID", uniStr);
			ds.setField("VALUE", regsin);
			ds.setField("PRIVATE_KEY", keyPairToString(keyPair.getPrivate()));
			ds.setField("PUBLIC_KEY", keyPairToString(keyPair.getPublic()));
			ds.update(sql);

		} catch (Exception e) {
			return null;
		}

		return uniStr;
	}

	public static void main(String[] args) {
		resignNewKey("Webdano");
	}
}
