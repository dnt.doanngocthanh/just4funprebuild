package com.dano.j4F.sevice;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class IPAddress {
	public static void main(String[] args) {
		System.out.println("IPv4 Address: " + getIPv4Address(getLocalhostIp()));
		System.out.println("IPv6 Address: " + getIPv6Address(getLocalhostIp()));
	}

	public static InetAddress getLocalhostIp() {
		try {
			return InetAddress.getLocalHost();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	private static String getIPv4Address(InetAddress inetAddress) {
		if (inetAddress != null) {
			return inetAddress.getHostAddress();
		}
		return null;
	}

	private static String getIPv6Address(InetAddress inetAddress) {
		if (inetAddress != null) {
			// Kiểm tra nếu địa chỉ IPv6 là localhost (::1), trả về null
			if (inetAddress.isLoopbackAddress()) {
				return null;
			}
			// Kiểm tra nếu địa chỉ IPv6 không được hỗ trợ, trả về null
			if (!inetAddress.getHostAddress().contains(":")) {
				return null;
			}
			return inetAddress.getHostAddress();
		}
		return null;
	}
}