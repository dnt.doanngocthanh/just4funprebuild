package com.dano.j4F.sevice;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ObjectUtils;

import com.dano.j4F.jdbc.DataSet;
import com.dano.j4F.jdbc.batchUpdateDataSet;

public abstract class DB_Module {
	public static String dataname_db = ConfigManager.getProperty("com.dano.j4F.jdbc.DataSet.dataname_db");
	public static String username_db = ConfigManager.getProperty("com.dano.j4F.jdbc.DataSet.username_db");
	public static String password_db = ConfigManager.getProperty("com.dano.j4F.jdbc.DataSet.password_db");
	public static String hostname_db = ConfigManager.getProperty("com.dano.j4F.jdbc.DataSet.hostname_db");
	private Class<?> classExtend;
	public static DataSet getDataSet() {
		DataSet DataSet = new DataSet();
		DataSet.setDB_DATABASE_NAME(dataname_db);
		DataSet.setDB_USERNAME(username_db);
		DataSet.setDB_PASSWORD(password_db);
		DataSet.setHOSTNAME(hostname_db);
		return DataSet;
	}

	public static batchUpdateDataSet getBatchUpdateDataSet() {
		String url = "jdbc:mysql://" + hostname_db + ":3306/" + dataname_db
				+ "?useSSL=false&useUnicode=yes&characterEncoding=UTF-8";
		batchUpdateDataSet buds = new batchUpdateDataSet(url, username_db, password_db);
		return buds;
	}

	@SuppressWarnings("deprecation")
	public synchronized static String get999config(String key) {
		DataSet ds = getDataSet();
		ds.setField("PARAM_NAME", key);
		List<Map<String, Object>> li = ds
				.searchAndRetrieve("Select PARAM_VALUE FROM A999_CONFIG WHERE PARAM_NAME=:PARAM_NAME");
		if (li != null) {
			if (li.size() > 0) {
				for (Map<String, Object> map : li) {
					return ObjectUtils.toString(map.get("param_value"), "");
				}
			}
		}

		return null;
	}

	public static int set999config(String key, String value) {
		DataSet ds = getDataSet();
		ds.setField("PARAM_NAME", key);
		ds.setField("PARAM_VALUE", value);
		return ds.update(
				"INSERT INTO DB_FORUM.A999_CONFIG (PARAM_NAME, PARAM_VALUE, DESCRIPTION) VALUES(:PARAM_NAME, :PARAM_VALUE, NULL);");
	}

	public static boolean isExitsTable(String Table) {
		boolean check = true;
		DataSet ds = getDataSet();
		ds.setField("TABLE", Table);
		if (ds.searchAndRetrieve("SHOW TABLES LIKE :TABLE").size() > 0) {
			check = false;
		}
		ds.setField("TABLE", Table.toLowerCase());
		if (ds.searchAndRetrieve("SHOW TABLES LIKE :TABLE").size() == 0) {
			check = true;
		}
		return check;
	}

	public static void main(String[] args) {
		System.out.println(DB_Module.set999config("thanh", "1223"));

//		batchUpdateDataSet buds = getBatchUpdateDataSet();
//		try {
//			String sql = "DELETE FROM A999_CONFIG WHERE PARAM_NAME=:ID";
//			// String sql = "INSERT INTO A999_CONFIG" + "(PARAM_NAME, PARAM_VALUE)" +
//			// "VALUES(:ID, 'POST_TITLE0');";
//
//			buds.setUpBatchInfo(batchUpdateDataSet.class);
//			buds.beginTransaction();
//			buds.preparedBatch(sql);
//			List<Map> li = new ArrayList();
//			for (int i = 0; i < 110; i++) {
//				Map<String, Object> map = new HashMap<>();
//				map.put("ID", i);
//				buds.setQuery(map);
//				buds.addBatch();
//			}
//			System.out.println(buds.executeBatch());
//
//		} finally {
//			buds.endTransaction();
//		}
	}

}
