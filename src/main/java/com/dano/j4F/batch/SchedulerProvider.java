package com.dano.j4F.batch;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;

public class SchedulerProvider {
	private static Scheduler scheduler;
	static {
		try {
			SchedulerFactory schedulerFactory = new StdSchedulerFactory();
			SchedulerListener jhb = new SchedulerListener();
			scheduler = schedulerFactory.getScheduler();
			if (!scheduler.isStarted()) {
				scheduler.start();
			}
			scheduler.getListenerManager().addJobListener(jhb);
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}

	public static Scheduler getScheduler() {
		return scheduler;
	}

}
