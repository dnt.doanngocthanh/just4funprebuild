package com.dano.j4F.batch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.quartz.CronScheduleBuilder;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.Trigger.TriggerState;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;

import com.dano.j4F.plugin.VoTooL;
import com.dano.j4F.plugin.classInfo;

public class batchExcute {
	private JobKey jobKey;
	private TriggerKey triggerKey;
	private TriggerState triggerState;
	private JobDetail jobDetail;
	private Trigger trigger;
	@SuppressWarnings("rawtypes")
	private Class<? extends Job> classBathExcute;
	private static Scheduler scheduler;
	private String cron_expression;
	private String cron_description;
	private String classBathExcuteString;
	private String jobID;
	private String jobName;
	private Map<Object, Object> jobData;
	private Map<Object, Object> returnMap;
	private batchExpression batchExp;
	@SuppressWarnings("rawtypes")

	private boolean runOnce = false;

	private static Map<Object, Object> mapConfigExcuteMap;
	static {
		// BasicConfigurator.configure();
		System.out.println("===============batchExcute===============");
		scheduler = SchedulerProvider.getScheduler();
		System.out.println("===============batchExcute===============");
		mapConfigExcuteMap = new HashMap<Object, Object>();
		mapConfigExcuteMap.put(-1, "Có lỗi xảy ra: ");
		mapConfigExcuteMap.put(0, "Thao tác thành công!");
		mapConfigExcuteMap.put(1, "Batch này đã được kích hoạt");
		mapConfigExcuteMap.put(2, "Không tìm thấy class batch được gọi");
		mapConfigExcuteMap.put(3, "Batch đã được Hold");
		mapConfigExcuteMap.put(4, "Batch đã được hủy kích hoạt");
		mapConfigExcuteMap.put(99, "Khởi tạo");
		mapConfigExcuteMap.put(100, "Không xác định");
	}

	public void returnMap(int returnCode, Exception e) {
		returnMap.put("returnCode", returnCode);
		returnMap.put("returnMess", mapConfigExcuteMap.get(returnCode));
		if (e != null) {
			returnMap.put("returnMess", mapConfigExcuteMap.get(returnCode) + " " + e.getMessage());
		}

	}

	public void onLog() {
		jobData.put("NEED_LOG", "Y");
	}

	public void onNotify() {
		jobData.put("NEED_NOTIFY", "Y");
	}

	public void offLog() {
		jobData.put("NEED_LOG", "N");
	}

	public void offNotify() {
		jobData.put("NEED_NOTIFY", "N");
	}

	public batchExcute() {
		batchExp = new batchExpression();
		returnMap = new HashMap();
		returnMap(99, null);
		jobData = new HashMap();
		jobData.put("NEED_LOG", "N");
		jobData.put("NEED_NOTIFY", "N");
		jobData.put("TYPE", "SYS");

	}

	public String getCron_expression() {
		return cron_expression;
	}

	public void setCron_expression(String cron_expression) {
		this.cron_expression = cron_expression;
	}

	public String getCron_description() {
		return cron_description;
	}

	public void setCron_description(String cron_description) {
		this.cron_description = cron_description;
	}

	public String getClassBathExcuteString() {
		return classBathExcuteString;
	}

	public void setClassBathExcuteString(String classBathExcuteString) {
		this.classBathExcuteString = classBathExcuteString;
	}

	public String getJobID() {
		return jobID;
	}

	public void setJobID(String jobID) {
		this.jobID = jobID;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public void runOnce() {
		this.runOnce = true;
	}

	public void addData(Map<Object, Object> map) {
		this.jobData.putAll(map);

	}

	public void addData(String key, Object value) {
		this.jobData.put(key, value);

	}

	public void scheduleJob() throws SchedulerException {
		jobKey = new JobKey(jobName);

		if (!scheduler.checkExists(jobKey)) {
			String jsonData;
			jsonData = new VoTooL().getJSONString(jobData);
			jobDetail = JobBuilder.newJob(classBathExcute).withIdentity(jobKey).usingJobData("jsonData", jsonData)
					.build();
			TriggerBuilder<Trigger> triggerBuilder = TriggerBuilder.newTrigger().withIdentity(jobName);

			if (runOnce) {
				trigger = triggerBuilder.startNow().build();
			} else {
				trigger = triggerBuilder.withSchedule(CronScheduleBuilder.cronSchedule(cron_expression)).build();
			}

			scheduler.scheduleJob(jobDetail, trigger);
			returnMap.put("jobName", jobName);
			returnMap.put("status", getTriggerState(jobName));
			returnMap(0, null);
		} else {
			returnMap(1, null);
			returnMap.put("jobName", jobName);
			returnMap.put("status", getTriggerState(jobName));
		}
	}

	public List<Map> excuteBatchApi(batchExpression b) {
		List<Map> listOutPutMap = new ArrayList<Map>();
		try {
			classBathExcute = (Class<? extends Job>) Class.forName(b.getClassNeedRun());
		
			if (!checkSchedule(classBathExcute.getSimpleName())) {
				setJobName(classBathExcute.getName());
				try {
					setCron_expression(b.getCronExpression());
				} catch (Exception e) {
					returnMap(-1, e);
					listOutPutMap.add(returnMap);
					return listOutPutMap;
				}
				setCron_description(b.getDescription());
				scheduleJob();
			} else {
				returnMap(1, null);
			}

		} catch (ClassNotFoundException e1) {
			returnMap(2, e1);
		} catch (SchedulerException e) {
			returnMap(-1, e);
		}
		listOutPutMap.add(returnMap);
		return listOutPutMap;
	}

	public List<Map> excuteBatchApi(String packageClass, String cron_expression, String dess) {
		List<Map> listOutPutMap = new ArrayList<Map>();
		try {
			classBathExcute = (Class<? extends Job>) Class.forName(packageClass);
			String projectName = classInfo.getProjectName(classBathExcute);
			System.out.println("Project name: " + projectName);
			if (!checkSchedule(classBathExcute.getSimpleName())) {
				setJobName(classBathExcute.getName());
				try {
					setCron_expression(cron_expression);
				} catch (Exception e) {
					returnMap(-1, e);
					listOutPutMap.add(returnMap);
					return listOutPutMap;
				}
				setCron_description(dess);
				scheduleJob();
			} else {
				returnMap(1, null);
			}

		} catch (ClassNotFoundException e1) {
			returnMap(2, e1);
		} catch (SchedulerException e) {
			returnMap(-1, e);
		}
		listOutPutMap.add(returnMap);
		return listOutPutMap;
	}

	public boolean checkSchedule(String jobName) throws SchedulerException {
		triggerKey = TriggerKey.triggerKey(jobName);
		triggerState = scheduler.getTriggerState(triggerKey);
		if (triggerState == TriggerState.NORMAL) {
			return true;
		} else {
			return false;
		}
	}

	public TriggerState getTriggerState(String jobName) {
		triggerKey = TriggerKey.triggerKey(jobName);
		try {
			triggerState = scheduler.getTriggerState(triggerKey);
		} catch (SchedulerException e) {
			return TriggerState.ERROR;
		}
		return triggerState;
	}

	public static void main(String[] args) {
		batchExcute ex = new batchExcute();
		Map map = new HashMap();
		ex.onLog();
		ex.addData("afff", "test");
		ex.addData("456", "dđ");
		// ex.runOnce();

		System.out.println(ex.excuteBatchApi("com.dano.j4F.batch.testbatch", "0/10 * * * * ?", ""));
		ex.runOnce = true;
//		System.out.println(ex.excuteBatchApi("com.dano.batch.SYS_LOG", "0/10 * * * * ?", ""));
//		System.out.println(ex.excuteBatchApi("com.dano.batch.SYS_LOG", "0/10 * * * * ?", ""));

	}

}
