package com.dano.j4F.batch;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobListener;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.Trigger.TriggerState;

public class SchedulerListener implements JobListener {
	private TriggerState triggerState;
	private JobExecutionException exception_;
	private JobExecutionContext context;
	private Trigger trigger;
	private String jobName;
	private String jobKey;
	private String jobGroup;
	private Class<?> jobClass;

	public TriggerState getTriggerState() {
		try {
			return context.getScheduler().getTriggerState(trigger.getKey());
		} catch (SchedulerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return TriggerState.ERROR;
	}

	public JobExecutionException getException_() {
		return exception_;
	}

	public void setException_(JobExecutionException exception_) {
		this.exception_ = exception_;
	}

	public JobExecutionContext getContext() {
		return context;
	}

	public void setContext(JobExecutionContext context) {
		this.context = context;
		this.jobClass = context.getJobDetail().getJobClass();
	}

	private int stopJob() {
		try {
			trigger = context.getTrigger();
			context.getScheduler().unscheduleJob(trigger.getKey());
			return 0;
		} catch (SchedulerException e) {
			// TODO Auto-generated catch block
		}
		return -1;
	}

	public SchedulerListener() {

	}

	@Override
	public String getName() {
		return this.getClass().getName();
	}

	@Override
	public void jobExecutionVetoed(JobExecutionContext context) {
		// Không xử lý trạng thái ở đây
	}

	@Override
	public synchronized void jobToBeExecuted(JobExecutionContext context) {
		setContext(context);
		handing();
	}

	@Override
	public synchronized void jobWasExecuted(JobExecutionContext context, JobExecutionException exception) {
		setContext(context);
		setException_(exception);
		handing();
	}

	public void handing() {
		
	}

	private TriggerState getTriggerState(JobExecutionContext context, Trigger trigger) {
		try {
			return context.getScheduler().getTriggerState(trigger.getKey());
		} catch (Exception e) {
			return TriggerState.ERROR;
		}
	}

}