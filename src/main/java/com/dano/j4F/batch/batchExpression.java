package com.dano.j4F.batch;

import java.util.Calendar;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import com.cronutils.descriptor.CronDescriptor;
import com.cronutils.model.Cron;
import com.cronutils.model.CronType;
import com.cronutils.model.definition.CronDefinition;
import com.cronutils.model.definition.CronDefinitionBuilder;
import com.cronutils.parser.CronParser;

public class batchExpression {
	private String second = "*";
	private String minute = "*";
	private String hour = "*";
	private String dayOfMonth = "*";
	private String month = "*";
	private String dayOfWeek = "*";
	private CronDefinition cronDefinition;
	private CronDescriptor descriptor;
	private Cron cron;
	private CronParser parser;
	private static Logger log = Logger.getLogger(batchExpression.class);
	private String classNeedRun;
	private String description;
	private boolean is_log = false;

	public batchExpression() {
		cronDefinition = CronDefinitionBuilder.instanceDefinitionFor(CronType.QUARTZ);
		parser = new CronParser(cronDefinition);
		descriptor = CronDescriptor.instance();

	}

	public void onLog() {
		this.is_log = true;
	}

	public String getClassNeedRun() {
		return classNeedRun;
	}

	public void setClassNeedRun(String classNeedRun) {
		this.classNeedRun = classNeedRun;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSecond() {
		return second;
	}

	public void setSecond(String second) {
		if (second != null && !second.isEmpty()) {
			this.second = second;
		}
	}

	public void setEverySecond(String second) {
		if (second != null && !second.isEmpty()) {
			this.second = "0/" + second;
		}
	}

	public String getMinute() {
		return minute;
	}

	public void setMinute(String minute) {
		if (minute != null && !minute.isEmpty()) {
			this.minute = minute;
		}
	}

	public String getHour() {
		return hour;
	}

	public void setHour(String hour) {
		if (hour != null && !hour.isEmpty()) {
			this.hour = hour;
		}
	}

	public String getDayOfMonth() {
		return dayOfMonth;
	}

	public void setDayOfMonth(String dayOfMonth) {
		if (dayOfMonth != null && !dayOfMonth.isEmpty()) {
			this.dayOfMonth = dayOfMonth;
		}

	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {

		if (month != null && !month.isEmpty()) {
			this.month = month;
		}
	}

	public String getDayOfWeek() {
		return dayOfWeek;
	}

	public void setDayOfWeek(String dayOfWeek) {
		if (dayOfWeek != null && !dayOfWeek.isEmpty()) {
			this.dayOfWeek = dayOfWeek;
		}
	}

	public String getSecondTime() {
		return "0/" + second + " * * * * ?";

	}

	public String cronExpressionNocheck() {
		return String.format("%s %s %s %s %s %s", second, minute, hour, dayOfMonth, month, dayOfWeek);
	}

	public String getCronEveryDayAtHour(String hour, String minute, String second) throws Exception {
		setHour(hour);
		setMinute(minute);
		setSecond(second);

		return getCronExpression();
	}

	public String getCronExpression() throws Exception {
		if (dayOfMonth != null && !dayOfMonth.isEmpty()) {
			if (!dayOfMonth.equals("*")) {
				dayOfWeek = "?";
			} else {
				dayOfMonth = "?";
			}

		}
		if (dayOfWeek != null && !dayOfWeek.isEmpty()) {
			if (!dayOfWeek.equals("*")) {
				if (!dayOfMonth.equals("?")) {
					dayOfWeek = "?";
				}
			}

		}

		try {
			cron = parser.parse(cronExpressionNocheck());
			String translatedExpression = descriptor.describe(cron);
			if (is_log) {
				log.debug("============================================");
				log.debug("Cron String   : " + cronExpressionNocheck());
				log.debug("Cron Translate: " + translatedExpression);
				log.debug("============================================");
			}

			return cronExpressionNocheck();
		} catch (Exception e) {
			log.error("Cron invalid: " + e.getMessage());
			throw e;

		}
	}

	public boolean checkcronExpression(String cronExpression) throws Exception {
		try {
			cron = parser.parse(cronExpression);
			descriptor.describe(cron);
			return true;
		} catch (Exception e) {
			throw e;
		}

	}

	public static void main(String[] args) {
		batchExpression b = new batchExpression();
		b.setEverySecond("20");
		b.setClassNeedRun("com.dano.j4F.batch.testbatch");
		b.setDescription("Test");
		b.onLog();
		try {
			// System.out.println(b.getCronEveryDayAtHour("09", "30", "00"));
			System.out.println(new batchExcute().excuteBatchApi(b));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// System.out.println(new batchExcute().excuteBatchApi(b));

	}

	public Calendar getCalendar(int minute, int hour, int dayOfMonth, int month, int dayOfWeek) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.MINUTE, minute);
		calendar.set(Calendar.HOUR_OF_DAY, hour);
		calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
		calendar.set(Calendar.MONTH, month);
		calendar.set(Calendar.DAY_OF_WEEK, dayOfWeek);
		return calendar;
	}

}
