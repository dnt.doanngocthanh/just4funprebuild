package com.dano.j4F.batch;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;

import com.dano.j4F.common.TIMESTAMP;
import com.dano.j4F.config.Enviroment;
import com.dano.j4F.firebase.RealTimeDB;
import com.dano.j4F.plugin.VoTooL;
import com.dano.j4F.sevice.ConfigManager;

public abstract class batchBean implements Job {
	protected JobExecutionContext context;
	protected String jSONString;
	protected VoTooL voTool;
	protected JobDataMap jobDataMap;
	protected String previousStatus;
	protected String jobName;
	protected String groupName;
	protected boolean is_next = false;
	protected Class<?> JobClass;
	protected TIMESTAMP timestamp;
	protected int status;
	protected Map<String, String> statusMap;
	protected RealTimeDB firebase;
	protected String firebaseKey = ConfigManager.getProperty("com.dano.j4F.batch.batchBean.getFireBaseConfig");

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public batchBean() {
		try {
			firebase = new RealTimeDB();
			firebase = firebase.getFireBaseConfig(firebaseKey);
		} catch (Exception e) {
			firebase = null;
			System.out.println("noconfig Firebase");
		}
		voTool = new VoTooL();
		timestamp = new TIMESTAMP();
	}

	public void nextIfError() {
		this.is_next = true;
	}

	public boolean isNextIfError() {
		return is_next;
	}

	protected void setContext(JobExecutionContext context) {
		this.context = context;
	}

	protected synchronized String getJSONString() {
		this.jSONString = context.getJobDetail().getJobDataMap().getString("jsonData");
		return jSONString;
	}

	@SuppressWarnings("unchecked")
	protected Map<Object, Object> getMap() {
		return voTool.stringJSONToMap(getJSONString());
	}

	protected List<Map<String, Object>> getListMap() {
		return voTool.stringJSONToListMapObj(getJSONString());
	}

	protected void setLOG(JobExecutionContext context) {
		JobClass = context.getJobDetail().getJobClass();
		this.setSYS_ID(JobClass.getSimpleName());
		this.setSYS_NAME(JobClass.getName());
		this.setSYS_GROUP(JobClass.getPackage().getName());
		this.setSYS_TIME(timestamp.getCurrentTime());
	}

	public Map getMapStatus(String Status) {
		statusMap = new HashMap<String, String>();
		statusMap.put("SYS_ID", getSYS_ID());
		statusMap.put("SYS_NAME", getSYS_NAME());
		statusMap.put("SYS_GROUP", getSYS_GROUP());
		statusMap.put("SYS_TIME", getSYS_TIME());
		statusMap.put("STATUS", Status);
		return statusMap;
	}

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {

		try {
			BEGIN(context);

			synchronized (context) {
				setLOG(context);
				this.status = 2;
				EXE(context);
				jobName = context.getJobDetail().getKey().getName();
			}
		} catch (Exception e) {
			ERROR(e, context);
		} finally {
			END(context);
		}
	}

	protected boolean check(String status) {
		return this.previousStatus.equals(status);
	}

	protected void BEGIN(JobExecutionContext context) {
		setLOG(context);
		try {
			if (firebase != null) {
				firebase.saveBatchStatusRealTime(getSYS_ID(), getMapStatus("START"));
			}

		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	};

	protected abstract void EXE(JobExecutionContext context) throws JobExecutionException;

	protected void END(JobExecutionContext context) {
		JobClass = context.getJobDetail().getJobClass();
		this.setSYS_ID(JobClass.getSimpleName());
		this.setSYS_NAME(JobClass.getName());
		this.setSYS_GROUP(JobClass.getPackage().getName());
		this.setSYS_TIME(timestamp.getCurrentTime());
		try {
			if (getStatus() < 0) {

			} else {
				if (firebase != null) {
					firebase.saveBatchStatusRealTime(getSYS_ID(), getMapStatus("DONE"));
				}
			}

		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	};

	protected String SYS_ID;
	protected String SYS_NAME;
	protected String SYS_GROUP;
	protected String SYS_EROR;
	protected String SYS_TIME;

	public String getSYS_ID() {
		return SYS_ID;
	}

	public void setSYS_ID(String sYS_ID) {
		SYS_ID = sYS_ID;
	}

	public String getSYS_NAME() {
		return SYS_NAME;
	}

	public void setSYS_NAME(String sYS_NAME) {
		SYS_NAME = sYS_NAME;
	}

	public String getSYS_GROUP() {
		return SYS_GROUP;
	}

	public void setSYS_GROUP(String sYS_GROUP) {
		SYS_GROUP = sYS_GROUP;
	}

	public String getSYS_EROR() {
		return SYS_EROR;
	}

	public void setSYS_EROR(String sYS_EROR) {
		SYS_EROR = sYS_EROR;
	}

	public String getSYS_TIME() {
		return SYS_TIME;
	}

	public void setSYS_TIME(String sYS_TIME) {
		SYS_TIME = sYS_TIME;
	}

	protected void writeLogEror() {
		setSYS_ID(JobClass.getSimpleName());
		setSYS_NAME(JobClass.getName());
		setSYS_GROUP(JobClass.getPackage().getName());
		setSYS_TIME(timestamp.getCurrentTime());

	}

	protected void ERROR(Exception e, JobExecutionContext context) {
		try {
			if (firebase != null) {
				firebase.saveBatchStatusRealTime(getSYS_ID(), getMapStatus("ERROR"));
				this.status = -1;
			}
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		Enviroment.notyfiDefault.sendMessage("[ERROR]: " + getSYS_ID() + " Message:" + e.getMessage());
		if (isNextIfError()) {

		} else {
			Scheduler scheduler = context.getScheduler();
			try {
				scheduler.unscheduleJob(context.getTrigger().getKey());
			} catch (SchedulerException e1) {
				e1.printStackTrace();
			}

		}
//		BATCH.USTATUS(this);
	};

}