package com.hibernate.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "zprofile_user_skill")
public class zprofile_user_skill {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_skill_id")
	private int userSkillId;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private zprofile_user_skill user;

	@ManyToOne
	@JoinColumn(name = "skill_id")
	private zprofile_skill skill;

	@Column(name = "skill_rating")
	private int skillRating;

	public int getUserSkillId() {
		return userSkillId;
	}

	public void setUserSkillId(int userSkillId) {
		this.userSkillId = userSkillId;
	}

	public zprofile_user_skill getUser() {
		return user;
	}

	public void setUser(zprofile_user_skill user) {
		this.user = user;
	}

	public zprofile_skill getSkill() {
		return skill;
	}

	public void setSkill(zprofile_skill skill) {
		this.skill = skill;
	}

	public int getSkillRating() {
		return skillRating;
	}

	public void setSkillRating(int skillRating) {
		this.skillRating = skillRating;
	}

}
