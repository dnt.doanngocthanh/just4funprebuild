package com.hibernate.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "sys_file_location_delete")
public class sys_file_location_delete {
	@Id
	@Column(name = "file_name", length = 100)
	private String file_name;
	@Column(name = "file_location", length = 100)
	private String file_location;
	@Column(name = "file_is_delete", length = 100)
	private String file_is_delete;
	@Column(name = "file_create_date", length = 1)
	private String file_create_date;
	@Column(name = "funtion_very", length = 15)
	private String funtion_very;
	@Column(name = "is_very_status", length = 1)
	private String is_very_status;
	@Column(name = "status", length = 1)
	private String status;
	public String getFile_name() {
		return file_name;
	}
	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}
	public String getFile_location() {
		return file_location;
	}
	public void setFile_location(String file_location) {
		this.file_location = file_location;
	}
	public String getFile_is_delete() {
		return file_is_delete;
	}
	public void setFile_is_delete(String file_is_delete) {
		this.file_is_delete = file_is_delete;
	}
	public String getFile_create_date() {
		return file_create_date;
	}
	public void setFile_create_date(String file_create_date) {
		this.file_create_date = file_create_date;
	}
	public String getFuntion_very() {
		return funtion_very;
	}
	public void setFuntion_very(String funtion_very) {
		this.funtion_very = funtion_very;
	}
	public String getIs_very_status() {
		return is_very_status;
	}
	public void setIs_very_status(String is_very_status) {
		this.is_very_status = is_very_status;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	

}
