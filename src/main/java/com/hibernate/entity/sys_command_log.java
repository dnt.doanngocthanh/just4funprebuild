package com.hibernate.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "sys_command_log")
public class sys_command_log {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "time_excute", length = 15)
	private String time_excute;
	@Column(name = "command_log")
	private String command_log;
	@Column(name = "return_log")
	private String return_log;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTime_excute() {
		return time_excute;
	}

	public void setTime_excute(String time_excute) {
		this.time_excute = time_excute;
	}

	public String getCommand_log() {
		return command_log;
	}

	public void setCommand_log(String command_log) {
		this.command_log = command_log;
	}

	public String getReturn_log() {
		return return_log;
	}

	public void setReturn_log(String return_log) {
		this.return_log = return_log;
	}
}
