package com.hibernate.config;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import com.fasterxml.jackson.databind.ObjectMapper;

public class OSet {
	private Configuration configuration;
	private SessionFactory sessionFactory;
	private String drive = "mysql";
	private static Map<String, String> mapDriver = new HashMap<String, String>();
	public List<String> listClass = new ArrayList<String>();
	static {
		mapDriver.put("mysql", "com.mysql.jdbc.Driver");
	}

	public void setDriver(String db_driver) {
		configuration.setProperty("hibernate.connection.driver_class", mapDriver.get(db_driver));
		this.drive = db_driver;
	}

	private void setConnection(String username, String password, String host, String port, String database) {
		configuration.setProperty("hibernate.connection.url",
				"jdbc:" + drive + "://" + host + ":" + port + "/" + database+"?useSSL=false&useUnicode=yes&characterEncoding=UTF-8");
		configuration.setProperty("hibernate.connection.username", username);
		configuration.setProperty("hibernate.connection.password", password);
		configuration.setProperty("hibernate.dialect", "org.hibernate.dialect.MariaDBDialect");
	}

	public static OSet initC(String username, String password, String host, String port, String database) {
		OSet os = new OSet();
		os.setConnection(username, password, host, port, database);
		os.configuration.setProperty("hibernate.show_sql", "true");
		os.configuration.setProperty("hibernate.hbm2ddl.auto", "create");
		os.init();
		return os;
	}

	public static OSet initU(String username, String password, String host, String port, String database) {
		OSet os = new OSet();
		os.setConnection(username, password, host, port, database);
		os.configuration.setProperty("hibernate.show_sql", "true");
		os.configuration.setProperty("hibernate.hbm2ddl.auto", "update");
		os.init();
		return os;
	}

	private void init() {
		if (this.sessionFactory != null) {
			this.sessionFactory.close();
		}
		StandardServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
				.applySettings(this.configuration.getProperties()).build();
		this.sessionFactory = this.configuration.buildSessionFactory(serviceRegistry);
	}

	public void addClass(String className) {
		boolean is_next = true;
		for (String string : listClass) {
			if (string.equals(className)) {
				is_next = false;
			}
		}
		if (is_next) {
			this.listClass.add(className);
		}
	}

	public void resignAll() {
		Class<?> classForName = null;
		String entityPackage = "com.hibernate.entity";
		for (String fileName : listClass) {
			try {
				classForName = Class.forName(entityPackage + "." + fileName);
				this.configuration.addAnnotatedClass(classForName);
			} catch (ClassNotFoundException e) {

			}
		}
		init();
	}

	public OSet() {
		this.configuration = new Configuration();
	}

	public void updateSessionFactory() {
		this.sessionFactory.close();
		init();
	}

	public SessionFactory getSessionFactory() {
		updateSessionFactory();
		return this.sessionFactory;
	}

	public boolean save(Object obj) {
		SessionFactory sessionFactory = getSessionFactory();
		Session session = sessionFactory.openSession();
		try {
			session.beginTransaction();
			session.save(obj);
			session.getTransaction().commit();
			return true;
		} catch (Exception e) {
			System.err.println("Exception occurred while performing database operations:");
			e.printStackTrace();
		} finally {
			session.close();
			sessionFactory.close();
		}
		return false;
	}

	public Map<String, Object> queryById(Class<?> entityClass, Serializable id) {
		SessionFactory sessionFactory = getSessionFactory();
		Session session = sessionFactory.openSession();
		try {
			session.beginTransaction();
			Object obj = session.get(entityClass, id);
			session.getTransaction().commit();

			if (obj != null) {
				ObjectMapper objectMapper = new ObjectMapper();
				return objectMapper.convertValue(obj, Map.class);
			}
		} catch (Exception e) {
			System.err.println("Exception occurred while performing database operations:");
			e.printStackTrace();
		} finally {
			session.close();
		}
		return null;
	}

	public List<Object> queryAllData(Class<?> entityClass, int fetchSize) {
		SessionFactory sessionFactory = getSessionFactory();
		Session session = sessionFactory.openSession();
		try {
			CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
			CriteriaQuery<Object> criteriaQuery = (CriteriaQuery<Object>) criteriaBuilder.createQuery(entityClass);
			Root<Object> root = (Root<Object>) criteriaQuery.from(entityClass);
			criteriaQuery.select(root);

			List<Object> resultList = session.createQuery(criteriaQuery).setFirstResult(0).setMaxResults(fetchSize)
					.getResultList();

			return resultList;
		} catch (Exception e) {
			System.err.println("Exception occurred while performing database operations:");
			e.printStackTrace();
		} finally {
			session.close();
		}
		return null;
	}
}
