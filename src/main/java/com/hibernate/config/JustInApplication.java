package com.hibernate.config;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.core.sevice.DB_Module;
import com.hibernate.entity.sys_class_hibernate;

public class JustInApplication extends DB_Module {
	private static final String entityPackage = "com.hibernate.entity";

	public static void LocalRunRenew() {
		String folderPath = System.getProperty("user.dir") + "\\src\\main\\java\\"
				+ JustInApplication.class.getPackageName().replace(".", "\\").replace("config", "entity");
		File folder = new File(folderPath);
		System.out.println(folderPath);
		List<sys_class_hibernate> li = new ArrayList<sys_class_hibernate>();
		if (folder.exists() && folder.isDirectory()) {
			File[] files = folder.listFiles();
			if (files != null) {
				for (File file : files) {
					if (file.isFile()) {
						Class<?> classForName = null;
						try {
							String fileName = file.getName().replace(".java", "");
							classForName = Class.forName(entityPackage + "." + fileName);
							sys_class_hibernate sch = new sys_class_hibernate();
							sch.setClass_name(classForName.getSimpleName());
							sch.setPackage_name(classForName.getPackageName());
							li.add(sch);
						} catch (ClassNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}
				}

			}
		}
		OSet clearSysClass = getOsetC();
		clearSysClass.addClass("sys_class_hibernate");
		clearSysClass.resignAll();
		OSet osc = getOsetC();
		OSet osu = getOsetU();
		osu.addClass("sys_class_hibernate");
		osu.resignAll();
		for (sys_class_hibernate map : li) {
			if (!map.getClass_name().equals("sys_class_hibernate")) {
				osc.addClass(map.getClass_name());
			}
			osu.save(map);
		}

		osc.resignAll();

	}

	public static void LocalRunUpdate() {
		String folderPath = System.getProperty("user.dir") + "\\src\\"
				+ JustInApplication.class.getPackageName().replace(".", "\\").replace("config", "entity");
		File folder = new File(folderPath);

		List<sys_class_hibernate> li = new ArrayList<sys_class_hibernate>();
		if (folder.exists() && folder.isDirectory()) {
			File[] files = folder.listFiles();
			if (files != null) {
				for (File file : files) {
					if (file.isFile()) {
						Class<?> classForName = null;
						try {
							String fileName = file.getName().replace(".java", "");
							classForName = Class.forName(entityPackage + "." + fileName);
							sys_class_hibernate sch = new sys_class_hibernate();
							sch.setClass_name(classForName.getSimpleName());
							sch.setPackage_name(classForName.getPackageName());
							li.add(sch);
						} catch (ClassNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}
				}

			}
		}
		OSet clearSysClass = getOsetC();
		clearSysClass.addClass("sys_class_hibernate");
		clearSysClass.resignAll();
		OSet osc = getOsetU();
		OSet osu = getOsetU();
		osu.addClass("sys_class_hibernate");
		osu.resignAll();
		for (sys_class_hibernate map : li) {
			if (!map.getClass_name().equals("sys_class_hibernate")) {
				osc.addClass(map.getClass_name());
			}
			osu.save(map);
		}

		osc.resignAll();

	}

	public static void main(String[] args) {
		LocalRunRenew();
//		OSet clearSysClass = getOsetU();
//		clearSysClass.addClass("sys_class_hibernate");
//		clearSysClass.resignAll();
//		List<Object> li = clearSysClass.queryAllData(sys_class_hibernate.class, 10);
//		for (Object object : li) {
//			try {
//				System.out.println(new ObjectMapper().writeValueAsString(object));
//			} catch (JsonProcessingException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}

	}
}
