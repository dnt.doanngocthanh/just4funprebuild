package com.system.command;

import com.jcraft.jsch.JSchException;

public class Utils {

	public static UserClient userDefault;
	static {
		userDefault = new UserClient();
		userDefault.setHost("test");
		userDefault.setPassword("12345");
		userDefault.setUsername("abcdefght");
		userDefault.setType("ssh");
	}

	public static void main(String[] args) {
		try {
		
			RunCommand.executeCommand(userDefault.getSessionSSH(), "ping google.com");
		} catch (JSchException e) {
			
			e.printStackTrace();
		}
	}
}
