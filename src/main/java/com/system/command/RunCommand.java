package com.system.command;

import java.io.*;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

public class RunCommand {
	public static void executeCommand(Session session, String command) {
		try {
			Channel channel = session.openChannel("exec");
			((ChannelExec) channel).setCommand(command);

			channel.setInputStream(null);
			((ChannelExec) channel).setErrStream(System.err);

			InputStream inputStream = channel.getInputStream();
			channel.connect();

			BufferedReader input = new BufferedReader(new InputStreamReader(inputStream));
			String line;
			while ((line = input.readLine()) != null) {
				System.out.println(line);
			}
			input.close();

			channel.disconnect();
		} catch (IOException | JSchException e) {
			e.printStackTrace();
		}
	}

	public void Run(String username, String password, String command) {
		String[] cmd = { "/bin/bash", "-c", "echo " + password + " | sudo -S -u " + username + " " + command };
		try {
			Process pb = Runtime.getRuntime().exec(cmd);
			String line;
			BufferedReader input = new BufferedReader(new InputStreamReader(pb.getInputStream()));
			while ((line = input.readLine()) != null) {
				System.out.println(line);
			}
			input.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void RunWindow(String command) {
		String[] cmd = new String[] { "cmd.exe", "/c", command };
		System.out.println(cmd);
		try {
			Process pb = Runtime.getRuntime().exec(cmd);
			String line;
			BufferedReader input = new BufferedReader(new InputStreamReader(pb.getInputStream()));
			while ((line = input.readLine()) != null) {
				System.out.println(line);
			}
			input.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void executeCommandWithCredentials(String username, String password, String command) {
		String[] cmd = { "/bin/bash", "-c", "echo " + password + " | sudo -S -u " + username + " " + command };

		try {
			ProcessBuilder pb = new ProcessBuilder(cmd);
			pb.redirectErrorStream(true);
			Process process = pb.start();

			BufferedReader input = new BufferedReader(new InputStreamReader(process.getInputStream()));
			String line;
			while ((line = input.readLine()) != null) {
				System.out.println(line);
			}
			input.close();

			int exitCode = process.waitFor();
			System.out.println("Exit Code: " + exitCode);
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		RunCommand cmd = new RunCommand();
		cmd.RunWindow("tasklist | findstr ssh");
	}

}
