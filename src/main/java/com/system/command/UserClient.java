package com.system.command;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

public class UserClient {
	private String username;
	private String password;
	private String host;
	private String port;
	private String type;

	public UserClient() {

	}

	private String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	private String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	private String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Session getSessionSSH() throws JSchException {
		if (type == null || type.isEmpty()) {
			throw new JSchException("Permission densied because: type is null!");
		} else if (type.toLowerCase().startsWith("ssh")) {
			JSch jsch = new JSch();
			Session session = jsch.getSession(getUsername(), getHost());
			session.setPassword(getPassword());
			session.setConfig("StrictHostKeyChecking", "no");
			session.connect();
			if (session.isConnected()) {
				return session;
			} else {
				return null;
			}
		} else {
			throw new JSchException("Permission densied because: type is wrong!");
		}

	}

}
