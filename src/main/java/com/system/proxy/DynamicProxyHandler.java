package com.system.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

interface MyInterface {
	void myMethod();
}

class MyObject implements MyInterface {
	public void myMethod() {
		System.out.println("Hello, World!");
	}
}

class DynamicProxyHandler implements InvocationHandler {
	private Object proxied;

	public DynamicProxyHandler(Object proxied) {
		this.proxied = proxied;
	}

	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		System.out.println("**** proxy: " + proxy.getClass() + ", method: " + method + ", args: " + args);
		if (args != null)
			for (Object arg : args)
				System.out.println("  " + arg);
		return method.invoke(proxied, args);
	}

	public static void main(String[] args) {
		MyInterface realObject = new MyObject();
		MyInterface proxy = (MyInterface) Proxy.newProxyInstance(MyInterface.class.getClassLoader(),
				new Class[] { MyInterface.class }, new DynamicProxyHandler(realObject));
		proxy.myMethod();
	}
}
