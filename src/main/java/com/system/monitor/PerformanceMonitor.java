package com.system.monitor;

public abstract class PerformanceMonitor {
	private long startTime;
	private long endTime;

	// Phương thức trừu tượng để thực thi
	protected abstract void execute();

	// Phương thức cuối cùng để bắt đầu và kết thúc theo dõi
	public final void run() {
		startMonitor();
		execute();
		endMonitor();
	}

	// Phương thức để bắt đầu theo dõi
	protected void startMonitor() {
		startTime = System.currentTimeMillis();
		System.out.println("startTime: " + startTime);
	}

	// Phương thức để kết thúc theo dõi
	protected void endMonitor() {
		endTime = System.currentTimeMillis();
		System.out.println("endTime: " + endTime);
	}

	// Phương thức để lấy thời gian thực thi
	public long getExecutionTime() {
		return endTime - startTime;
	}
	
}
