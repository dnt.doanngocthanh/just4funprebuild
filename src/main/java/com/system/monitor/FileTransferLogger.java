package com.system.monitor;

import java.io.*;
import java.util.*;

public class FileTransferLogger {
	private File logFile;

	public FileTransferLogger(String logFilePath) {
		logFile = new File(logFilePath);
	}

	public void logTransfer(String fileName) {
		try (FileWriter writer = new FileWriter(logFile, true)) {
			String logEntry = new Date().toString() + ": File sent: " + fileName + "\n";
			writer.write(logEntry);
		} catch (IOException e) {
			System.out.println("Error writing to log file: " + e.getMessage());
		}
	}
}
