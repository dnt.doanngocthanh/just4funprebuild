package com.system.file;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Calendar;

public class DeleteFile {
	public static void cleanupFilesInMonth(String directoryPath) {
		File directory = new File(directoryPath);
		if (!directory.isDirectory()) {
			System.out.println("Đường dẫn không phải là một thư mục hợp lệ.");
			return;
		}

		File[] files = directory.listFiles();
		if (files == null) {
			System.out.println("Không thể đọc thư mục.");
			return;
		}

		Calendar calendar = Calendar.getInstance();
		int currentMonth = calendar.get(Calendar.MONTH);

		for (File file : files) {
			Path filePath = file.toPath();
			try {
				BasicFileAttributes attributes = Files.readAttributes(filePath, BasicFileAttributes.class);
				long fileCreationTime = attributes.creationTime().toMillis();
				calendar.setTimeInMillis(fileCreationTime);
				int fileMonth = calendar.get(Calendar.MONTH);

				if (fileMonth != currentMonth) {
					file.delete();
					System.out.println("Đã xóa tệp: " + file.getName());
				}
			} catch (Exception e) {
				System.out.println("Lỗi khi đọc thuộc tính của tệp: " + file.getName());
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {
		String directoryPath = "E:\\lab1"; // Thay thế bằng đường dẫn thư mục thực tế
		cleanupFilesInMonth(directoryPath);
	}
}
