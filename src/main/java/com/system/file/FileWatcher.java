package com.system.file;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;

public class FileWatcher {
	private final WatchService watcher;
	private final Path dir;

	public FileWatcher(Path dir) throws IOException {
		this.watcher = FileSystems.getDefault().newWatchService();
		this.dir = dir;
		this.dir.register(watcher, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE,
				StandardWatchEventKinds.ENTRY_MODIFY);
	}

	public void processEvents() {
		for (;;) {
			WatchKey key;
			try {
				key = watcher.take();
			} catch (InterruptedException x) {
				return;
			}

			for (WatchEvent<?> event : key.pollEvents()) {
				WatchEvent.Kind<?> kind = event.kind();

				if (kind == StandardWatchEventKinds.OVERFLOW) {
					continue;
				}

				WatchEvent<Path> ev = (WatchEvent<Path>) event;
				Path filename = ev.context();

				System.out.println(kind.name() + ": " + filename);
			}

			boolean valid = key.reset();
			if (!valid) {
				break;
			}
		}
	}

	public static void main(String[] args) {
		Path dir = Paths.get("E:\\");
		try {
			FileWatcher watcher = new FileWatcher(dir);
			watcher.processEvents();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
