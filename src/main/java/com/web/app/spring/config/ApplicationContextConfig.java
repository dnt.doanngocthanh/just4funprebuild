package com.web.app.spring.config;

import java.util.Properties;

import org.apache.log4j.PropertyConfigurator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration
@ComponentScan("com.web.app.spring.*")
public class ApplicationContextConfig {

	@Bean(name = "viewResolver")
	public InternalResourceViewResolver getViewResolver() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setPrefix("/WEB-INF/public/");
		viewResolver.setSuffix(".jsp");
		return viewResolver;
	}

	/*
	 * @Bean(name = "multipartResolver") public CommonsMultipartResolver
	 * commonsMultipartResolver() { final CommonsMultipartResolver
	 * commonsMultipartResolver = new CommonsMultipartResolver();
	 * commonsMultipartResolver.setMaxUploadSize(-1); return
	 * commonsMultipartResolver; }
	 */

	@Bean(name = "configLog4j")
	public PropertyConfigurator configLog4j() {
		PropertyConfigurator configurer = new PropertyConfigurator();
		Properties proper = new Properties();
		proper.setProperty("log4j.rootCategory", "DEBUG, all");
		proper.setProperty("log4j.appender.all", "org.apache.log4j.ConsoleAppender");
		proper.setProperty("log4j.appender.all.layout", "org.apache.log4j.PatternLayout");
		proper.setProperty("log4j.appender.all.layout.ConversionPattern",
				"%d{yyyy-MM-dd HH:mm:ss,SSS} [%-5p][%C{1}.%M()(%L)] - %m%n");
		configurer.configure(proper);
		return configurer;
	}
}
